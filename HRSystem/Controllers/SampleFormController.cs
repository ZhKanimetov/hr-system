﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using HRSystem.Models;
using HRSystem.Repositories;
using HRSystem.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HRSystem.Controllers
{
    public class SampleFormController : Controller
    {
        private readonly ISampleFormRepository sampleRepository;
        private readonly IHostingEnvironment environment;
        private readonly ISamoleKeyRepository samoleKeyRepository;
        private readonly ISampleFormRepository sampleFormRepository;

        public SampleFormController(ISampleFormRepository sampleRepository, IHostingEnvironment environment, ISamoleKeyRepository samoleKeyRepository, ISampleFormRepository sampleFormRepository)
        {
            this.sampleRepository = sampleRepository;
            this.environment = environment;
            this.samoleKeyRepository = samoleKeyRepository;
            this.sampleFormRepository = sampleFormRepository;
        }

        public IActionResult Index()
        {
            List<SampleForm> samples = sampleRepository.GetAllEntities().ToList();
            return View(samples);
        }

        public IActionResult Create()
        {
            return PartialView();
        }
        [HttpPost]
        public IActionResult Create(SampleFormViewModel model)
        {
            sampleRepository.AddSample(model);
            return RedirectToAction("Index");
        }

        public IActionResult Info(int id)
        {
            SampleForm sample = sampleRepository.GetEntityById(id);
            string type = sample.Path.Split(".").Last();
            string sourceFile = Path.Combine(environment.WebRootPath, sample.Path);
            return PhysicalFile(sourceFile, $"application/{type}", sample.Name + $".{type}");
        }

        public IActionResult EmployeerSample(int sampleId, int employeerId)
        {
            SampleForm sample = sampleRepository.GetEntityById(sampleId);
            string type = sample.Path.Split(".").Last();
            string path;
            if (type.Equals("txt"))
            {
                path = sampleRepository.GetEmployeerSampleTxt(sampleId, employeerId);
            }
            else
            {
                path = sampleRepository.GetEmployeerSampleDocx(sampleId, employeerId);
            }
            return PhysicalFile(path, $"application/{type}", sample.Name + $".{type}");
        }

        [HttpPost]
        public string Delete(int sampleId)
        {
            sampleRepository.DeleteSample(sampleId);
            return JsonConvert.SerializeObject(true,
                new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
        }

        public IActionResult SampleKeys()
        {
            List<SampleKey> sampleKeys = samoleKeyRepository.GetAllEntities().ToList();
            return PartialView(sampleKeys);
        }

        public IActionResult EditKey(int id)
        {
            SampleKey sampleKey = samoleKeyRepository.GetEntityById(id);
            return PartialView(sampleKey);
        }
        [HttpPost]
        public IActionResult EditKey(SampleKey model)
        {
            SampleKey sampleKey = samoleKeyRepository.GetEntityById(model.Id);
            sampleFormRepository.UpdateDocx(sampleKey.Key, model.Key);
            sampleKey.Key = model.Key;
            samoleKeyRepository.Update(sampleKey);
            return RedirectToAction("AdminSettings", "Home", new { type = "SampleKeys" });
        }
    }
}