﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HRSystem.Models;
using HRSystem.Repositories;
using HRSystem.ViewModels;
using Microsoft.AspNetCore.Mvc.ViewFeatures.Internal;
using System.Net.Mail;
using System.Net;
using HRSystem.Services;
using Microsoft.AspNetCore.Identity;

namespace HRSystem.Controllers
{
    public class HomeController : Controller
    {
        private readonly IEmployeerRepository employeerRepository;
        private readonly IDocumentRepository documentRepository;
        private readonly ICertificateRepository certificateRepository;
        private readonly EmailSender emailSender;
        private readonly UserManager<User> userManager;

        public HomeController(IEmployeerRepository employeerRepository, IDocumentRepository documentRepository, ICertificateRepository certificateRepository, EmailSender emailSender, UserManager<User> userManager)
        {
            this.employeerRepository = employeerRepository;
            this.documentRepository = documentRepository;
            this.certificateRepository = certificateRepository;
            this.emailSender = emailSender;
            this.userManager = userManager;
        }

        public IActionResult Index()
        {
            User user = userManager.GetUserAsync(this.User).Result;
            Employeer employeer = new Employeer();

            List<Employeer> futbirt = new List<Employeer>();
            List<Employeer> pastbirt = new List<Employeer>();
            if (!user.UserName.Equals("sisadmin"))
            {
                employeer = employeerRepository.Get(user.Id);
                futbirt = employeerRepository.FutureBirthDateNotification(employeer.Id);
                pastbirt = employeerRepository.PastBirthDateNotification(employeer.Id);
            }
            else
            {
                futbirt = employeerRepository.FutureBirthDateNotification(0);
                pastbirt = employeerRepository.PastBirthDateNotification(0);
            }
            NotificationViewModel model = new NotificationViewModel()
            {
                futureBirthdayEmployer = futbirt,
                pastBirtdayEmployer = pastbirt,
                FutureDocuments = documentRepository.FutureDocumentNotification(),
                PastDocuments = documentRepository.PastDocumentNotification(),
                FutureCertificates = certificateRepository.FutureCertificateNotification(),
                PastCertificates = certificateRepository.PastCertificateNotification()
            };
            
            return View(model);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }
    
    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Contact(EmailFormModel model)
    {
        if (ModelState.IsValid)
        {
            emailSender.Send(model);
                return RedirectToAction("Sent");
            
        }
        return View(model);
    }
    public ActionResult Sent()
    {
        return View();
    }

    public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult AdminSettings(string type)
        {
            ViewBag.Type = type;
            return View();
        }
    }
}
