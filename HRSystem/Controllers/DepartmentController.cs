﻿using System;
using System.Linq;
using System.Threading.Tasks;
using HRSystem.Models;
using HRSystem.Repositories;
using HRSystem.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace HRSystem.Controllers
{
    public class DepartmentController : Controller
    {
        private UserManager<User> userManager;
        private readonly IDepartmentRepository departmentRepository;

        public DepartmentController(
            UserManager<User> userManager,
            IDepartmentRepository departmentRepository)
        {
            this.userManager = userManager;
            this.departmentRepository = departmentRepository;
        }

        public IActionResult Index()
        {
            var model = departmentRepository.GetAllEntities();
            return PartialView(model);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return PartialView();
        }

        [HttpPost]
        public IActionResult Create(Department department)
        {
            if (ModelState.IsValid)
            {
                department = departmentRepository.AddEntity(department);
               
            }
            return RedirectToAction("AdminSettings", "Home", new { type = "Department" });
        }
        
        [HttpPost]
        public IActionResult Delete(int id)
        {
            if (ModelState.IsValid)
            {
               departmentRepository.RemoveEntity(id);
               
            }

            return RedirectToAction("Index");
        }
        [HttpGet]
        public IActionResult Edit(int id)
        {
            Department department = departmentRepository.GetEntityById(id);
            return PartialView(new DepartmentViewModel()
            {
                Name =  department.Name,
                Id = department.Id
            });
        }

        [HttpPost]
        public IActionResult Edit(Department department)
        {
            departmentRepository.Update(department);
            return RedirectToAction("AdminSettings", "Home", new { type = "Department" });
        }
    }
}