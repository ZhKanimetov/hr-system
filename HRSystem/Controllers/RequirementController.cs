﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HRSystem.Models;
using HRSystem.Repositories;
using HRSystem.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace HRSystem.Controllers
{
    public class RequirementController : Controller
    {
        private UserManager<User> userManager;
        private readonly IRequirementRepository requirementRepository;

        public RequirementController(
            UserManager<User> userManager,
            IRequirementRepository requirementRepository)
        {
            this.userManager = userManager;
            this.requirementRepository = requirementRepository;
        }

        public IActionResult Index()
        {
            var model =  requirementRepository.GetAllEntities();
            return PartialView(model);
        }


        [HttpGet]
        public IActionResult Create()
        {
            return PartialView();
        }

        [HttpPost]
        public IActionResult Create(RequirementViewModel model)
        {
            if (ModelState.IsValid)
            {
                    Requirement requirement = new Requirement
                    {
                        Name = model.Name,
                        Validaty = model.Validaty.HasValue ? model.Validaty.Value : 0
                    };
                requirement = requirementRepository.AddEntity(requirement);

            }
            return RedirectToAction("AdminSettings", "Home");
        }

        [HttpPost]
        public IActionResult Delete(int id)
        {
            if (ModelState.IsValid)
            {
                requirementRepository.RemoveEntity(id);

            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            Requirement requirement = requirementRepository.GetEntityById(id);
            return PartialView(new RequirementViewModel()
            {
                Name = requirement.Name,
                Validaty = requirement.Validaty,
                Id = requirement.Id
            });
        }

        [HttpPost]
        public IActionResult Edit(Requirement requirement)
        {
            requirementRepository.Update(requirement);
            return RedirectToAction("AdminSettings", "Home");
        }
    }
}