﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using HRSystem.Models;
using HRSystem.Repositories;
using HRSystem.ViewModels;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Drawing;
using System.IO;
using System.Net.Mime;
using System.Text.RegularExpressions;
using HRSystem.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore.ChangeTracking.Internal;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json;

namespace HRSystem.Controllers
{
    public enum Status
    {
        AtWork,
        Dismissed,
        OnVacation
    }
    [Authorize(Roles = "Admin, User")]
    public class EmployeerController : Controller
    {
        private readonly IEmployeerRepository employeerRepository;
        private readonly IDepartmentRepository departmentRepository;
        private readonly IPositionRepository positionRepository;
        private readonly IPosReqRepository posReqRepository;
        private readonly IRelationRepository relationRepository;
        private readonly ISkillRepository skillRepository;
        private readonly IDocumentRepository documentRepository;
        private readonly ICertificateRepository certificateRepository;
        private readonly ISampleFormRepository sampleFormRepository;
        private readonly EmailSender emailSender;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IRequirementRepository requirementRepository;

        public EmployeerController(
            IEmployeerRepository employeerRepository,
            IDepartmentRepository departmentRepository,
            IPositionRepository positionRepository,
            IPosReqRepository posReqRepository,
            IRelationRepository relationRepository,
            ISkillRepository skillRepository,
            IDocumentRepository documentRepository,
            ICertificateRepository certificateRepository,
            ISampleFormRepository sampleFormRepository,
            EmailSender emailSender, UserManager<User> userManager, SignInManager<User> signInManager,
            IRequirementRepository requirementRepository)
        {
            this.employeerRepository = employeerRepository;
            this.departmentRepository = departmentRepository;
            this.positionRepository = positionRepository;
            this.posReqRepository = posReqRepository;
            this.relationRepository = relationRepository;
            this.skillRepository = skillRepository;
            this.documentRepository = documentRepository;
            this.certificateRepository = certificateRepository;
            this.sampleFormRepository = sampleFormRepository;
            this.emailSender = emailSender;
            _userManager = userManager;
            _signInManager = signInManager;
            this.requirementRepository = requirementRepository;
        }

        public IActionResult Index(List<string> requirements)
        {
            var model = skillRepository.FilterByRequirements(requirements);
            ViewBag.Department = departmentRepository.GetAllEntities().ToList();
            ViewBag.Positions = positionRepository.GetAllEntities().ToList();
            ViewBag.Requirement = requirementRepository.GetAllEntities();
            ViewBag.Search = requirements;
            return View(model);
        }

        [HttpGet]
        public IActionResult Create()
        {
            ViewBag.Department = departmentRepository.GetAllEntities().ToList();
            return View();
        }

        public IActionResult PosList(int id, int positionId)
        {
            id = -1 == id ? departmentRepository.GetFirst().Id : id;
            ViewBag.Positions = positionRepository.GetDepartmentPositions(id).ToList();
            ViewBag.Id = positionId;
            return PartialView();
        }

        public IActionResult SkillList(int positionId, int departmentId, int employeerId)
        {
            positionId = -1 == positionId ? positionRepository.GetFirst().Id : positionId;
            if (departmentId != 0)
            {
                try
                {
                    Position position = positionRepository.GetDepartmentPositions(departmentId).First();
                    positionId = position.Id;
                }
                catch (InvalidOperationException ex)
                {
                    Console.WriteLine(ex);
                    positionId = 0;
                }


            }
            ViewBag.Requirements = posReqRepository.FindPositionRequirements(positionId, employeerId);
            return PartialView();
        }

        [HttpPost]
        public async Task<IActionResult> Create(EmployeerViewModel model)
        {
            Employeer employeer = employeerRepository.AddEmployeer(model);


            relationRepository.AddRangeRelations(model.RelName, model.RelType, model.RelBirhday, employeer.Id);
            skillRepository.AddRangeSkills(model.Skills, employeer.Id);
            documentRepository.AddDocuments(model.DocTitle, model.DocFile, employeer.Id, model.StartDateDoc, model.EndDateDoc);
            certificateRepository.AddCertificates(model.CerTitle, model.CerFile, employeer.Id, model.StartDateCer, model.EndDateCer);

            

            var users = new User { UserName = model.Email.First(), Email = model.Email.First() };
            string usersPWD = SendLoginAndPassword(employeer.Id);
            var createuser = await _userManager.CreateAsync(users, usersPWD);
            var user = await _userManager.FindByNameAsync(users.UserName);
            employeer.IdForPersonal = user.Id;
            if (createuser.Succeeded)
            {
                await _userManager.AddToRoleAsync(users, "User");              
            }
          


            return RedirectToAction("EmployeerInfo", new { id = employeer.Id });
        }

        public IActionResult EmployeerInfo(int id)
        {
            var model = new InfoViewModel()
            {
                Employeer = employeerRepository.GetEntityById(id),
                Relations = relationRepository.GetRelationsByEmployeerId(id).ToList(),
                Certificates = certificateRepository.GetCertificatesByEmployeerId(id).ToList(),
                Documents = documentRepository.GetDocumentsByEmploueerId(id),
                Requirements = skillRepository.GetSkillsByEmployeerId(id).ToList(),
                SampleForms = sampleFormRepository.GetAllEntities().ToList()
            };
            return View(model);
        }

        public IActionResult EmployeerShortInfo(int id)
        {
            return PartialView(employeerRepository.GetEntityById(id));
        }

        [HttpPost]
        public IActionResult Delete(int id)
        {
            if (ModelState.IsValid)
            {
                employeerRepository.RemoveEntity(id);

            }

            return RedirectToAction("Index");
        }

        [Authorize(Roles = "User, Admin")]
        public IActionResult UpdatePesonalInfo(int id)
        {
            EmployeerViewModel model = employeerRepository.GetViewModel(id);
            ViewBag.Department = departmentRepository.GetAllEntities().ToList();
            return View(model);
        }
        [Authorize(Roles = "User, Admin")]
        [HttpPost]
        public IActionResult UpdatePesonalInfo(EmployeerViewModel model)
        {
            employeerRepository.UpdateEmployeerViewModel(model);
            return RedirectToAction("EmployeerInfo", new { id = model.Id });
        }
        [Authorize(Roles = "User, Admin")]
        public string GetInfo(int id)
        {
            Employeer employeer = employeerRepository.GetEntityById(id);
            return JsonConvert.SerializeObject(string.Join("#", employeer.Telephone, employeer.Email),
                new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
        }

        //public string GetCerInfo(int id)
        //{
        //    string cer = certificateRepository.GetArray(id);
        //    return JsonConvert.SerializeObject(cer,
        //        new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
        //}

        //public string GetDocInfo(int id)
        //{
        //    string cer = documentRepository.GetArray(id);
        //    return JsonConvert.SerializeObject(cer,
        //        new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
        //}
        [Authorize(Roles = "User, Admin")]
        [HttpPost]
        public string ChangeAvatar(int id)
        {
            string path = employeerRepository.SaveImage(Request.Form.Files.First(), id);
            return JsonConvert.SerializeObject(path,
                new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
        }


        [Authorize(Roles = "Admin")]
        public IActionResult UpdatePosition(int id, int departmentId, int positionId)
        {
            ViewBag.Department = departmentRepository.GetAllEntities().ToList();
            return View(new EmployeerViewModel()
            {
                Id = id,
                DepartmentId = departmentId,
                PositionId = positionId
            });
        }
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public IActionResult UpdatePosition(EmployeerViewModel model)
        {
            employeerRepository.UpdatePosition(model.DepartmentId, model.PositionId, model.Id);
            skillRepository.UpdateSkills(model.Skills, model.Id);

            return RedirectToAction("EmployeerInfo", new { id = model.Id });
        }
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public string UpdateStatus(int id, string status)
        {
            return employeerRepository.UpdateStatus(id, status);
        }
        [Authorize(Roles = "User, Admin")]
        public IActionResult GetHistory(int id)
        {
            return View(skillRepository.GetHistory(id));
        }
        [Authorize(Roles = "User, Admin")]
        public IActionResult UpdatePassport(int id)
        {
            return PartialView(employeerRepository.GetPassport(id));
        }
        [Authorize(Roles = "User, Admin")]
        [HttpPost]
        public IActionResult UpdatePassport(PassportViewModel model)
        {
            employeerRepository.UpdatePassport(model);
            return RedirectToAction("EmployeerInfo", new { id = model.EmployeerId });
        }
        [Authorize(Roles = "User, Admin")]
        [HttpPost]
        public string DeleteRelative(int relId)
        {
            relationRepository.RemoveEntity(relId);
            return JsonConvert.SerializeObject(true,
                new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
        }
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public string DeleteDocument(int docId)
        {
            documentRepository.DeleteFile(docId);
            return JsonConvert.SerializeObject(true,
                new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
        }
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public string DeleteCertificate(int cerId)
        {
            certificateRepository.DeleteFile(cerId);
            return JsonConvert.SerializeObject(true,
                new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
        }
        [Authorize(Roles = "User, Admin")]
        public IActionResult UpdateRelative(int id)
        {
            return PartialView(new RelativeViewModel()
            {
                EmployeerId = id
            });
        }
        [Authorize(Roles = "User, Admin")]
        [HttpPost]
        public IActionResult UpdateRelative(RelativeViewModel model)
        {
            relationRepository.AddEntity(new Relation()
            {
                Birthday = model.Birthday,
                EmployeerId = model.EmployeerId,
                Name = model.Name,
                Type = model.Type
            });
            return RedirectToAction("EmployeerInfo", new { id = model.EmployeerId });
        }
        [Authorize(Roles = "Admin")]
        public IActionResult UpdateDocument(int id)
        {
            return PartialView(new DocumentViewModel()
            {
                EmployeerId = id
            });
        }
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public IActionResult UpdateDocument(DocumentViewModel model)
        {
            documentRepository.AddDocuments(new List<string>() { model.Title }, new List<IFormFile>() { model.DocumentImage }, model.EmployeerId, new List<DateTime>() { model.StartDateDoc }, new List<DateTime?> { model.EndDateDoc });
            return RedirectToAction("EmployeerInfo", new { id = model.EmployeerId });
        }
        [Authorize(Roles = "Admin")]
        public IActionResult UpdateCertificate(int id)
        {
            return PartialView(new CertificateViewModel()
            {
                EmployeerId = id
            });
        }
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public IActionResult UpdateCertificate(CertificateViewModel model)
        {
            certificateRepository.AddCertificates(new List<string>() { model.Title }, new List<IFormFile>() { model.CertificateImage }, model.EmployeerId, new List<DateTime>() { model.StartDateCer }, new List<DateTime?>() { model.EndDateCer });
            return RedirectToAction("EmployeerInfo", new { id = model.EmployeerId });
        }




        [Authorize(Roles = "Admin, User")]
        public IActionResult Download(string type, int id)
        {
            List<string> result = new List<string>();
            switch (type)
            {
                case "Certificate":
                    result = certificateRepository.Download(id);
                    return PhysicalFile(result[0], result[1], result[2]);
                case "Document":
                    result = documentRepository.Download(id);
                    return PhysicalFile(result[0], result[1], result[2]);
                default:
                    return new NotFoundResult();
            }
        }
        [Authorize(Roles = "Admin")]
        public string SendLoginAndPassword(int id)
        {
            Random generator = new Random();
            String r = generator.Next(0, 999999).ToString("D6");

            Employeer employeer = employeerRepository.GetEntityById(id);
            List<string> emails = employeer.Email.Split(",", StringSplitOptions.RemoveEmptyEntries).ToList();
            foreach (var email in emails)
            {
                emailSender.Send(new EmailFormModel()
                {
                    FromEmail = email,
                    Message = $"Login: {emails.First()} Password: {r}",
                    ThemeMessage = $"{DateTime.Now}"
                });
            }

            return r;
        }
        [Authorize(Roles = "User, Admin")]
        public async Task<IActionResult> UserInfo()
        {
            User user = await _userManager.GetUserAsync(User);
            Employeer employeer = employeerRepository.FindByEmail(user.Id);
            return RedirectToAction("EmployeerInfo", new {id = employeer.Id});
        }

    }
}