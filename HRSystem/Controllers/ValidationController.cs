﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HRSystem.Models;
using HRSystem.Repositories;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace HRSystem.Controllers
{
    public class ValidationController : Controller
    {
        private UserManager<User> userManager;
        private readonly IDepartmentRepository departmentRepository;
        private readonly IPositionRepository positionRepository;
        private readonly IRequirementRepository requirementRepository;
        private readonly IEmployeerRepository employeerRepository;

        public ValidationController(
            UserManager<User> userManager,
            IDepartmentRepository departmentRepository,IPositionRepository positionRepository, IRequirementRepository requirementRepository ,IEmployeerRepository employeerRepository)
        {
            this.userManager = userManager;
            this.departmentRepository = departmentRepository;
            this.positionRepository = positionRepository;
            this.requirementRepository = requirementRepository;
            this.employeerRepository = employeerRepository;
        }
        
        [AcceptVerbs("Get", "Post")]
        public IActionResult DepartmentValidation(string name, int id)
        {
            Department department = departmentRepository.FindByName(name);
            Department department2 = id != 0 ? departmentRepository.GetEntityById(id) : new Department();

            if (department2.Name != name && department != null)
            {
                return Json("Такой отдел уже существует");
            }

            return Json(true);
        }

        [AcceptVerbs("Get", "Post")]
        public IActionResult RequirementValidation(string name, int id)
        {
            Requirement requirement = requirementRepository.FindByName(name);
            Requirement requirement2 = id != 0 ? requirementRepository.GetEntityById(id) : new Requirement();
            if (requirement2.Name != name && requirement != null)
            {
                return Json("Такая компетенция уже существует");
            }

            return Json(true);
        }

        [AcceptVerbs("Get", "Post")]
        public IActionResult PositionValidation(string name, int id)
        {
            Position position = positionRepository.FindByName(name);
            Position position2 = id != 0 ? positionRepository.GetEntityById(id) : new Position();

            if (position2.Name != name && position  != null)
            {
                return Json("Такая должность уже существует");
            }

            return Json(true);
        }

        [AcceptVerbs("Get", "Post")]
        public IActionResult EmployeerValidation(string name)
        {
            Employeer employeer = employeerRepository.FindByName(name);

            if (employeer != null)
            {
                return Json("Такой сотрудниу уже имеется");
            }

            return Json(true);
        }

        [AcceptVerbs("Get", "Post")]
        public IActionResult CheckValidity(int validaty)
        {
            if(validaty != 0)
            {
                if(validaty < 1)
                {
                    return Json("Неправильное значение");
                }
            }

            return Json(true);
        }


    }
}