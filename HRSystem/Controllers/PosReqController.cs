﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HRSystem.Models;
using HRSystem.Repositories;
using HRSystem.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace HRSystem.Controllers
{
    public class PosReqController : Controller
    {
        private readonly IPosReqRepository posReqRepository;
        private readonly IRequirementRepository requirementRepository;

        public PosReqController(IPosReqRepository posReqRepository, IRequirementRepository requirementRepository)
        {
            this.posReqRepository = posReqRepository;
            this.requirementRepository = requirementRepository;
        }

        public IActionResult Index(int positionId)
        {
            List<PositionRequirement> posRequirements = posReqRepository.GetPositionRequirements(positionId);
            ViewBag.PositionId = positionId;
            return PartialView(posRequirements);
        }

        public IActionResult CreatePosReq(int positionId)
        {
            return View(new PosReqViewModel()
            {
                AllRequirements = requirementRepository.GetAllEntities().ToList(),
                ImpRequirements = posReqRepository.FindPositionRequirements(positionId,true),
                NoImpRequirements = posReqRepository.FindPositionRequirements(positionId, false),
                PositionId = positionId
            });
        }

        [HttpPost]
        public IActionResult CreatePosReq(int positionId,List<string> requirements,List<string> importance)
        {
            posReqRepository.AddToPositionRequiremets(positionId,requirements,importance);
            return RedirectToAction("AdminSettings", "Home", new { type = "Position" });
        }
    }
}