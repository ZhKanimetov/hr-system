﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HRSystem.Models;
using HRSystem.Repositories;
using HRSystem.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace HRSystem.Controllers
{
    public class PositionController : Controller
    {
        private readonly IPositionRepository positionRepository;
        private readonly IDepartmentRepository departmentRepository;

        public PositionController(IPositionRepository positionRepository, IDepartmentRepository departmentRepository)
        {
            this.positionRepository = positionRepository;
            this.departmentRepository = departmentRepository;
        }

        public IActionResult Index()
        {
            List<Position> positions = positionRepository.GetAllEntities().ToList();
            return PartialView(positions);
        }
        public IActionResult PositionCreate()
        {
            ViewBag.Departments = departmentRepository.GetAllEntities().ToList();
            return PartialView();
        }

        [HttpPost]
        public IActionResult PositionCreate(Position position)
        {

            positionRepository.AddEntity(position);
            return RedirectToAction("AdminSettings", "Home", new { type = "Position" });
        }

        [HttpPost]
        public IActionResult DeletePosition(int id)
        {
            if (ModelState.IsValid)
            {
                positionRepository.RemoveEntity(id);

            }

            return RedirectToAction("Index");
        }
        [HttpGet]
        public IActionResult Edit(int id)
        {
            List<Department> departments = departmentRepository.GetAllEntities().ToList();
            ViewBag.Departaments = new SelectList(departments, "Id", "Name");
            Position position = positionRepository.GetEntityById(id);
            return PartialView(new PositionViewModel()
            {
                Name = position.Name,
                DepartmentId = position.DepartmentId,
                Id = position.Id
            });
        }

        [HttpPost]
        public IActionResult Edit(Position position)
        {
            positionRepository.Update(position);
            return RedirectToAction("AdminSettings", "Home", new {type = "Position"});
        }
    }
}