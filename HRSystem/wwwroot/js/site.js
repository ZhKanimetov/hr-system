﻿

function initMask() {
    $(".phone").mask("0 (999) 999-999");
}

function initDateMask() {
    $(".datesss").mask("99.99.9999");
}

$(document).ready(function () {
    $("#add_field_button").click(function (e) {
        e.preventDefault();
        $("#input_fields_wrap").append('<div class="form-inline"><input class="phone form-control" type="text" id="Telephone" name="Telephone" value=""><a href="#" class="remove_field">Удалить</a><span class="text-danger field-validation-valid" data-valmsg-for="Telephone" data-valmsg-replace="true"></span></div>');
        initMask();
    });

    $("#input_fields_wrap").on("click", ".remove_field", function (e) {
        e.preventDefault();
        $(this).parent("div").remove();
    });
});
$(document).ready(function () {
    $("#add_email_button").click(function (e) {
        e.preventDefault();
        $(".input_email_wrap").append('<div class="form-inline"><input class="form-control" type="text" id="Email" name="Email" value><a href="#" class="remove_email">Удалить</a><span class="text-danger field-validation-valid" data-valmsg-for="Email" data-valmsg-replace="true"></span></div>');
    });
    $(".input_email_wrap").on("click", ".remove_email", function (e) {
        e.preventDefault();
        $(this).parent('div').remove();
    });
});
$(document).ready(function () {
    $("#add_document_button").click(function (e) {
        var inTitle = '<div class="form-group"><input type="text" class="form-control" placeholder="Наименование" id="DocTitle" name="DocTitle" value=""></div> ';
        var inImg = '<div class="form-group"><input class="form-control" type="file" name="DocFile" accept="*/*"></div> ';
        var inDocValidity = '<div class="form-group"><input type="text" class="form-control" placeholder="Срок годности" id="DocTitle" name="DocumentValidity" value=""></div> ';
        var rmv = '<button class="btn btn-default remove_document" >-</button><span class="text-danger field-validation-valid" data-valmsg-for="DocTitle" data-valmsg-replace="true"></span><span class="text-danger field-validation-valid" data-valmsg-for="DocFile" data-valmsg-replace="true"></span>';
        e.preventDefault();
        $("#input_document_wrap").append('<div class="form-inline">' + inTitle + inImg + inDocValidity + rmv + "</div>");
        initDateMask();
    });

    $("#input_document_wrap").on("click", ".remove_document", function (e) {
        e.preventDefault();
        $(this).parent("div").remove();
    });
});
$(document).ready(function () {
    $("#add_certificate_button").click(function (e) {
        var inTitle = '<div class="form-group"><input type="text" class="form-control" placeholder="Наименование"  name="CerTitle" value=""></div> ';
        var inImg = '<div class="form-group"><input class="form-control" type="file" name="CerFile" accept="*/*"></div> ';
        var inCerValidity = '<div class="form-group"><input type="text" class="form-control" placeholder="Срок годности" id="DocTitle" name="CertificateValidity" value=""></div> ';
        var rmv = '<button class="btn btn-default remove_certificate" >-</button><span class="text-danger field-validation-valid" data-valmsg-for="CerTitle" data-valmsg-replace="true"></span><span class="text-danger field-validation-valid" data-valmsg-for="CerFile" data-valmsg-replace="true"></span>';
        e.preventDefault();
        $("#input_certificate_wrap").append('<div class="form-inline">' + inTitle + inImg + inCerValidity + rmv + '</div>');
    });

    $("#input_certificate_wrap").on("click", ".remove_certificate", function (e) {
        e.preventDefault();
        $(this).parent("div").remove();
    });
});
$(document).ready(function () {
    $("#add_reletive_button").click(function (e) {
        var inName = '<div class="col-md-4"><input   placeholder="ФИО" class="form-control" type="text"  name="RelName" value /><span class="text-danger field-validation-valid" data-valmsg-for="RelName" data-valmsg-replace="true"></span></div>';
        var inStatus = '<div class="col-md-3"><input  placeholder="Статус" class="form-control" type="text"  name="RelType" value /><span class="text-danger field-validation-valid" data-valmsg-for="RelType" data-valmsg-replace="true"></span></div>';
        var inBirhday = '<div class="col-md-3"><input placeholder="Год рождения" class="form-control date" type="text"  name="RelBirhday" value><span class="text-danger field-validation-valid" data-valmsg-for="RelBirhday" data-valmsg-replace="true"></span></div>';
        var rmv = '<div class="col-md-1"><button class="btn btn-default remove_reletive">-</button></div>';
        e.preventDefault();
        $("#input_reletive_wrap").append('<div>' + inName + inStatus + inBirhday + rmv + '</div>');
        initDateMask();
    });

    $("#input_reletive_wrap").on("click", ".remove_reletive", function (e) {
        e.preventDefault();
        $(this).parent("div").parent("div").remove();
    });
});



function getPhoto(path) {
    $("#myModalLabel").text("Файл");
    $("#updateContent").empty();
    $("#updateContent").append('<img src="/' + path + '" width="500" height="500"/>');
   
    $("#myModal").modal();
}

// function getSamp(path) {
//     $("#myModalLabel").text("Файл");
//     $("#updateContent").empty();
//     $("#updateContent").append('<textPath src="/' + path + '" width="500" height="500"/>');
//
//     $("#myModal").modal();
// }

$(document).ready(function () {
    $("#employeerTable").DataTable({
        "language": {
            "lengthMenu": "Показать _MENU_ сотрудников на странице",
            "zeroRecords": "Сотрудников нет",
            "info": "Страница _PAGE_ из _PAGES_",
            "infoEmpty": "Ничего не найдено",
            "infoFiltered": "( отфилтрована из _MAX_ сотрудников )",
            "search": "",
            "paginate": {
                "previous": "Назад",
                "next": "Вперед"
            },
            searchPlaceholder: "Поиск"
        }
    });
    

    $("#requirementTable").DataTable({
        "language": {
            "lengthMenu": "Показать _MENU_ компетенций на странице",
            "zeroRecords": "Компетенций нет",
            "info": "Страница _PAGE_ из _PAGES_",
            "infoEmpty": "Ничего не найдено",
            "infoFiltered": "( отфилтрована из _MAX_ компетенций )",
            "search": "",
            "paginate": {
                "previous": "Назад",
                "next": "Вперед"
            },
            searchPlaceholder: "Поиск"
        }
    });

    $("#departmentTable").DataTable({
        "language": {
            "lengthMenu": "Показать _MENU_ отделы на странице",
            "zeroRecords": "Отделов нет",
            "info": "Страница _PAGE_ из _PAGES_",
            "infoEmpty": "Ничего не найдено",
            "infoFiltered": "( отфилтрована из _MAX_ отделов )",
            "search": "",
            "paginate": {
                "previous": "Назад",
                "next": "Вперед"
            },
            searchPlaceholder: "Поиск"
        }
    });

    $("#positionTable").DataTable({
        "language": {
            "lengthMenu": "Показать _MENU_ должности на странице",
            "zeroRecords": "Должностей нет",
            "info": "Страница _PAGE_ из _PAGES_",
            "infoEmpty": "Ничего не найдено",
            "infoFiltered": "( отфилтрована из _MAX_ должностей )",
            "search": "",
            "paginate": {
                "previous": "Назад",
                "next": "Вперед"
            },
            searchPlaceholder: "Поиск"
        }
    });

    $("#keyTable").DataTable({
        "language": {
            "lengthMenu": "Показать _MENU_ ключей на странице",
            "zeroRecords": "Ключей нет",
            "info": "Страница _PAGE_ из _PAGES_",
            "infoEmpty": "Ничего не найдено",
            "infoFiltered": "( отфилтрована из _MAX_ ключей )",
            "search": "",
            "paginate": {
                "previous": "Назад",
                "next": "Вперед"
            },
            searchPlaceholder: "Поиск"
        }
    });


    $(".dataTables_filter input").addClass("form-control");
});
