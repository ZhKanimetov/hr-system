﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HRSystem.Models;
using HRSystem.Repositories;
using HRSystem.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;


namespace HRSystem
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            bool defaultTestUsing = Convert.ToBoolean(Configuration["DefaultBaseUsing"]);

            services.AddDbContext<ApplicationContext>(options =>
                options.UseSqlServer(
                    defaultTestUsing ?
                        Configuration.GetConnectionString("DefaultConnection") :
                        Configuration.GetConnectionString("TestConnection")
                )
            );

            services.AddIdentity<User, IdentityRole>(
                options =>
                {
                    options.Password.RequireDigit = false;
                    options.Password.RequireLowercase = false;
                    options.Password.RequireNonAlphanumeric = false;
                    options.Password.RequireUppercase = false;
                    options.Password.RequiredLength = 3;
                })
                .AddEntityFrameworkStores<ApplicationContext>();

            services.AddMvc();
            services.AddTransient<FileUploadService>();
            services.AddTransient<IDepartmentRepository, DepartmentRepository>();
            services.AddTransient<IRequirementRepository, RequirementRepository>();
            services.AddTransient<IPositionRepository, PositionRepository>();
            services.AddTransient<IEmployeerRepository, EmployeerRepository>();
            services.AddTransient<IPosReqRepository, PosReqRepository>();
            services.AddTransient<ISkillRepository, SkillRepository>();
            services.AddTransient<IRelationRepository, RelationRepository>();
            services.AddTransient<IDocumentRepository, DocumentRepository>();
            services.AddTransient<ICertificateRepository, CertificateRepository>();
            services.AddTransient<ISampleFormRepository, SampleFormRepository>();
            services.AddSingleton<IHostedService, NotificationService>();
            services.AddSingleton<ISamoleKeyRepository, SamoleKeyRepository>();
            services.AddSingleton<EmailSender>();
            return services.BuildServiceProvider();
        }

        public void Configure(IApplicationBuilder app)
        {
            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Account}/{action=Login}/{id?}");
            });
        }
    }
}
