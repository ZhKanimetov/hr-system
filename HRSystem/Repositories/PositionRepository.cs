﻿using HRSystem.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRSystem.Repositories
{
    public interface IPositionRepository : IRepository<Position>
    {
        Position FindByName(string name);
        IEnumerable<Position> GetDepartmentPositions(int id);
    }
    public class PositionRepository : Repository<Position>, IPositionRepository
    {
        public PositionRepository(ApplicationContext context) : base(context)
        {
            AllEntities = context.Positions;
        }

        public override IEnumerable<Position> GetAllEntities()
        {
            return AllEntities.Include(e => e.Department).ToList();
        }
        public Position FindByName(string name)
        {
            return AllEntities.FirstOrDefault(u => u.Name == name);
        }

        public IEnumerable<Position> GetDepartmentPositions(int id)
        {
            return AllEntities.Where(p => p.DepartmentId == id).ToList();
        }
    }
}
