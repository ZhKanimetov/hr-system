﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using HRSystem.Models;
using HRSystem.Services;
using HRSystem.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using SautinSoft.Document;

namespace HRSystem.Repositories
{
    public interface ISampleFormRepository : IRepository<SampleForm>
    {
        void AddSample(SampleFormViewModel model);
        void AddFile(IFormFile file);
        List<string> GetText(int id);
        string GetEmployeerSampleTxt(int sampleId, int employeerId);
        string GetEmployeerSampleDocx(int sampleId, int employeerId);
        void DeleteSample(int sampleId);
        void UpdateDocx(string oldKey, string newKey);
    }

    public class SampleFormRepository : Repository<SampleForm>, ISampleFormRepository
    {
        private readonly FileUploadService fileUploadService;
        private readonly IHostingEnvironment environment;
        private readonly IEmployeerRepository employeerRepository;
        private readonly ISamoleKeyRepository samoleKeyRepository;

        public SampleFormRepository(ApplicationContext context, FileUploadService fileUploadService, IHostingEnvironment environment, IEmployeerRepository employeerRepository, ISamoleKeyRepository samoleKeyRepository) : base(context)
        {
            this.fileUploadService = fileUploadService;
            this.environment = environment;
            this.employeerRepository = employeerRepository;
            this.samoleKeyRepository = samoleKeyRepository;
            AllEntities = context.SampleForms;
        }

        public void AddSample(SampleFormViewModel model)
        {
            AddEntity(new SampleForm()
            {
                Name = model.Name,
                Path = $"samples/{model.SampleFile.FileName}"
            });
            AddFile(model.SampleFile);
        }

        public void AddFile(IFormFile file)
        {
            string path = Path.Combine(
                environment.WebRootPath,
                "samples");

            fileUploadService.Upload(path, file.FileName, file);
        }

        public List<string> GetText(int id)
        {
            List<string> result = new List<string>();
            SampleForm sample = GetEntityById(id);
            string fullPath = Path.Combine(environment.WebRootPath, sample.Path);
            StreamReader file = new StreamReader(fullPath, Encoding.GetEncoding("windows-1251"));
            while ((fullPath = file.ReadLine()) != null)
            {
                result.Add(fullPath);
            }

            file.Close();
            return result;
        }

        public string GetEmployeerSampleTxt(int sampleId, int employeerId)
        {
            string path = CreateSampleForEmployee(sampleId, employeerId);
            List<string> texts = GetText(sampleId);
            Regex regex = new Regex(@"\$\$.*?\$\$");
            using (StreamWriter file = new StreamWriter(path))
            {
                for (int i = 0; i < texts.Count; i++)
                {
                    MatchCollection collections = regex.Matches(texts[i]);
                    if (!collections.Any())
                    {
                        file.WriteLine(texts[i]);
                        continue;
                    }
                    for (int j = 0; j < collections.Count; j++)
                    {
                        texts[i] = texts[i].Replace(collections[j].ToString(),
                            GetEmployeerInfo(employeerId, collections[j].ToString()));
                    }
                    file.WriteLine(texts[i]);
                }
            }

            return path;
        }

        public string GetEmployeerSampleDocx(int sampleId, int employeerId)
        {
            SampleForm sample = GetEntityById(sampleId);
            string sourceFile = Path.Combine(environment.WebRootPath, sample.Path);
            string destinationFile = Path.Combine(environment.WebRootPath, @"samples\document.docx");
            List<SampleKey> sampleKeys = samoleKeyRepository.GetAllEntities().ToList();
            DocumentCore dc = DocumentCore.Load(sourceFile);
            foreach (var sampleKey in sampleKeys)
            {
                string searchText = $"$${sampleKey.Key}$$";
                string replaceText = GetEmployeerInfo(employeerId, sampleKey.Value);
                
                foreach (ContentRange cr in dc.Content.Find(searchText).Reverse())
                {
                    cr.Replace(replaceText);
                }
            }
            
            dc.Save(destinationFile);
            

            return destinationFile;
        }

        public void DeleteSample(int sampleId)
        {
            SampleForm sample = GetEntityById(sampleId);
            fileUploadService.FileDelete(sample.Path);
            RemoveEntity(sampleId);
        }

        public void UpdateDocx(string oldKey, string newKey)
        {
            List<SampleForm> samples = AllEntities.ToList();
            foreach (var sample in samples)
            {
                string sourceFile = Path.Combine(environment.WebRootPath, sample.Path);
                DocumentCore dc = DocumentCore.Load(sourceFile);
                foreach (ContentRange cr in dc.Content.Find($"$${oldKey}$$").Reverse())
                {
                    cr.Replace($"$${newKey}$$");
                }
                dc.Save(sourceFile);
            }
        }

        private string GetEmployeerInfo(int employeerId, string type)
        {
            Employeer employeer = employeerRepository.GetEntityById(employeerId);
            var dictionary = employeer.GetType()
                .GetProperties(BindingFlags.Instance | BindingFlags.Public)
                .ToDictionary(prop => prop.Name, prop => prop.GetValue(employeer, null));


            type = type.Replace("$", String.Empty).Replace(" ", string.Empty);

            var result = dictionary.FirstOrDefault(d => d.Key.ToLower().Contains(type.ToLower()));
            if (result.Value == null || string.IsNullOrEmpty(type)) return "__________________";
            switch (type)
            {
                case "isMarried":
                    bool b = bool.Parse(result.Value.ToString());
                    return b ? "Женат / Замужем" : "Холост / Не замужем";
                case "isMilitaryService":
                    bool c = bool.Parse(result.Value.ToString());
                    return c ? "Служил" : "Не служил";
                case "isGender":
                    bool g = bool.Parse(result.Value.ToString());
                    return g ? "Мужчина" : "Женщина";
                default:
                    return DateTime.TryParse((result.Value).ToString(), out var date) ? $"{date:d}" : result.Value.ToString();
            }

            
        }

       
        private string CreateSampleForEmployee(int sampleId, int employeerId)
        {
            string targetPath = Path.Combine(environment.WebRootPath, @"samples\" + employeerId);

            if (!Directory.Exists(targetPath))
            {
                Directory.CreateDirectory(targetPath);
            }
            targetPath = Path.Combine(targetPath, sampleId + ".txt");
            if (!File.Exists(targetPath))
            {
                using (FileStream fs = File.Create(targetPath))
                {
                    for (byte i = 0; i < 100; i++)
                    {
                        fs.WriteByte(i);
                    }
                }
            }
            else
            {
                Console.WriteLine("File \"{0}\" already exists.", targetPath);
                return targetPath;
            }

            return targetPath;
        }
    }
}