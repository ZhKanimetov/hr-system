﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HRSystem.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace HRSystem.Repositories
{
    public interface IDepartmentRepository : IRepository<Department>
    {
        Department FindByName(string name);
    }
    
    public class DepartmentRepository : Repository<Department>, IDepartmentRepository
    {
        public DepartmentRepository(ApplicationContext context) : base(context)
        {
            AllEntities = context.Departments;
        }
        
        
        public Department FindByName(string name)
        {
            return AllEntities.FirstOrDefault(u => u.Name == name);
        }
    }
}