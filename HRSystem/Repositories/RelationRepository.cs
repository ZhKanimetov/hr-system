﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HRSystem.Models;
using HRSystem.ViewModels;

namespace HRSystem.Repositories
{
    public interface IRelationRepository : IRepository<Relation>
    {
        void AddRangeRelations(List<string> name, List<string> type, List<DateTime> birhthday, int employeerId);
        IEnumerable<Relation> GetRelationsByEmployeerId(int employeerId);
        IEnumerable<RelativeViewModel> GetViewModel(int employeerId);
        string GetArray(int employeerId);
    }

    public class RelationRepository : Repository<Relation>, IRelationRepository
    {
        public RelationRepository(ApplicationContext context) : base(context)
        {
            AllEntities = context.Relations;
        }

        public void AddRangeRelations(List<string> name, List<string> type, List<DateTime> birhthday, int employeerId)
        {
            if (name == null) return;
            int i = 0;
            foreach (var m in name)
            {
                if (string.IsNullOrEmpty(m)) continue;
                AddEntity(new Relation()
                {
                    Birthday = Convert.ToDateTime(birhthday[i]),
                    Name = m,
                    Type = type[i],
                    EmployeerId = employeerId
                });
                i++;
            }
        }

        public IEnumerable<Relation> GetRelationsByEmployeerId(int employeerId)
        {
            return AllEntities.Where(r => r.EmployeerId == employeerId).ToList();
        }

        public IEnumerable<RelativeViewModel> GetViewModel(int employeerId)
        {
            List<RelativeViewModel> model = new List<RelativeViewModel>();

            foreach (var relation in GetRelationsByEmployeerId(employeerId))
            {
                model.Add(new RelativeViewModel()
                {
                    Name = relation.Name,
                    Birthday = relation.Birthday,
                    Type = relation.Type
                });
            }

            return model;
        }

        public string GetArray(int employeerId)
        {
            List<Relation> relations = GetRelationsByEmployeerId(employeerId).ToList();
            relations.Remove(relations.First());
            var model = string.Empty;
            foreach (var t in relations)
            {
                model += t.Name + ",";
                model += t.Type + ",";
                model += t.Birthday.ToString();
                model += "!";
            }

            return model;
        }

        
    }
}
