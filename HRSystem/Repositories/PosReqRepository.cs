﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HRSystem.Models;
using Microsoft.EntityFrameworkCore;

namespace HRSystem.Repositories
{
    public interface IPosReqRepository : IRepository<PositionRequirement>
    {
        List<PositionRequirement> FindPositionRequirements(int positionId, int eployeerId = 0);
        List<Requirement> FindPositionRequirements(int positionId, bool isImportant);
        void AddToPositionRequiremets(int positionId, List<string> requirements, List<string> importance);
        List<PositionRequirement> GetPositionRequirements(int positionId);
    }
    public class PosReqRepository : Repository<PositionRequirement>, IPosReqRepository
    {
        private readonly IRequirementRepository requirementRepository;
        private readonly ISkillRepository skillRepository;

        public PosReqRepository(ApplicationContext context,
            IRequirementRepository requirementRepository, ISkillRepository skillRepository) : base(context)
        {
            AllEntities = context.PositionRequirements;
            this.requirementRepository = requirementRepository;
            this.skillRepository = skillRepository;
        }

        public PosReqRepository(ApplicationContext context, IRequirementRepository requirementRepository) : base(context)
        {
            this.requirementRepository = requirementRepository;
        }

        public List<PositionRequirement> FindPositionRequirements(int positionId,int eployeerId = 0)
        {
            List<PositionRequirement> requirements = AllEntities.Where(r => r.PositionId == positionId).Include(r => r.Requirement)
                .ToList();
            if (eployeerId == 0) return requirements;
            var rating = skillRepository.GetSkillsByEmployeerId(eployeerId).ToList();
            foreach (var req in rating)
            {
                PositionRequirement change = requirements.FirstOrDefault(r => r.RequirementId == req.RequirementId);
                if (change != null) change.Rating = req.Rating;
            }

            return requirements;
        }

        public List<Requirement> FindPositionRequirements(int positionId, bool isImportant)
        {
            return AllEntities.Where(r => r.PositionId == positionId && r.IsImportant == isImportant).Include(r => r.Requirement)
                .Select(r => r.Requirement).ToList();
        }

        public void AddToPositionRequiremets(int positionId, List<string> requirements, List<string> importance)
        {
            List<Requirement> posRequirements = FindPositionRequirements(positionId).Select(r => r.Requirement).ToList();
            foreach (var reqId in requirements.Union(importance))
            {
                Requirement requirement = requirementRepository.GetEntityById(Convert.ToInt32(reqId));
                if (!posRequirements.Contains(requirement))
                {
                    AddEntity(new PositionRequirement()
                    {
                        PositionId = positionId,
                        RequirementId = Convert.ToInt32(reqId),
                        IsImportant = importance.Contains(reqId)
                    });
                }
                else
                {
                    PositionRequirement positionRequirement = AllEntities.First(r => r.RequirementId == requirement.Id);
                    positionRequirement.IsImportant = importance.Contains(reqId);
                    Update(positionRequirement);
                    posRequirements.Remove(requirement);
                }
            }

            foreach (var req in posRequirements)
            {
                PositionRequirement posReq = AllEntities.First(r => r.RequirementId == req.Id);
                RemoveEntity(posReq.Id);
            }
        }

        public List<PositionRequirement> GetPositionRequirements(int positionId)
        {
            return AllEntities.Where(r => r.PositionId == positionId).Include(r => r.Requirement)
                .ToList();
        }
    }
}
