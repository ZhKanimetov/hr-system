﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using HRSystem.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace HRSystem.Repositories
{
    public interface IRepository<T> where T : Entity
    {
        T GetEntityById(int id);
        IEnumerable<T> GetAllEntities();
        T AddEntity(T entity);
        void RemoveEntity(int id);
        T GetFirst();
        void Update(T entity);
    }
    
    public class Repository <T> : IRepository<T> where T : Entity
    {
        private readonly ApplicationContext context;

        protected DbSet<T> AllEntities { get; set; }

        public Repository(ApplicationContext context)
        {
            this.context = context;
        }

        public virtual T GetEntityById(int id)
        {
 
                return AllEntities.FirstOrDefault(e => e.Id == id);
        }

        public virtual IEnumerable<T> GetAllEntities()
        {
            return AllEntities.ToList();
        }

        public T AddEntity(T entity)
        {
            var model = AllEntities.Add(entity).Entity;
            context.SaveChanges();
            return model;
        }

        public void RemoveEntity(int id)
        {
            var entity = GetEntityById(id);
            AllEntities.Remove(entity);
            context.SaveChanges();
        }

        public T GetFirst()
        {
            return AllEntities.FirstOrDefault();
        }

        public void Update(T entity)
        {
            AllEntities.Update(entity);
            context.SaveChanges();
        }
    }
}