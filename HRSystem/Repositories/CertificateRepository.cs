﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using HRSystem.Models;
using HRSystem.Services;
using HRSystem.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace HRSystem.Repositories
{
    public interface ICertificateRepository : IRepository<Certificate>
    {
        void AddCertificates(List<string> modelCerTitle, List<IFormFile> modelCerFile, int employeerId,
            List<DateTime> startDate, List<DateTime?> endDate);
        IEnumerable<Certificate> GetCertificatesByEmployeerId(int employeerId);
        IEnumerable<CertificateViewModel> GetViewModel(int id);
        //string GetArray(int id);
        void UpdateCertificateModel(List<CertificateViewModel> modelCertificates,int employeerId);
        void AddFile(IFormFile file, int id);
        void DeleteFile(int certId);
        List<Certificate> FutureCertificateNotification();
        List<Certificate> PastCertificateNotification();
        List<Certificate> Notifications();
        List<string> Download(int id);
    }

    public class CertificateRepository : Repository<Certificate>, ICertificateRepository
    {
        private readonly FileUploadService fileUploadService;
        private readonly IHostingEnvironment environment;

        public CertificateRepository(ApplicationContext context, FileUploadService fileUploadService, IHostingEnvironment environment) : base(context)
        {
            this.fileUploadService = fileUploadService;
            this.environment = environment;
            AllEntities = context.Certificates;
        }

        public void AddCertificates(List<string> modelCerTitle, List<IFormFile> modelCerFile, int employeerId, List<DateTime> startDate, List<DateTime?> endDate)
        {
            if (modelCerTitle == null) return;
            int i = 0;
            foreach (var cer in modelCerTitle)
            {
                if (string.IsNullOrEmpty(cer)) continue;
                AddEntity(new Certificate()
                {
                    EmployeerId = employeerId,
                    Title = cer,
                    Path = $"images/{employeerId}/certificates/{modelCerFile[i].FileName}",
                    EndDate = endDate[i],
                    StartDate = startDate[i]
                });
                AddFile(modelCerFile[i], employeerId);
                i++;
            }
        }

        public  void AddFile(IFormFile file, int id)
        {
            string path = Path.Combine(
                environment.WebRootPath,
                $"images\\{id}\\certificates");

            fileUploadService.Upload(path, file.FileName, file);
        }

        public void DeleteFile(int certId)
        {
            Certificate certificate = GetEntityById(certId);
            fileUploadService.FileDelete(certificate.Path);
            RemoveEntity(certId);
        }

        public IEnumerable<Certificate> GetCertificatesByEmployeerId(int employeerId)
        {
            return AllEntities.Where(c => c.EmployeerId == employeerId);
        }

        public IEnumerable<CertificateViewModel> GetViewModel(int id)
        {
            List<CertificateViewModel> model = new List<CertificateViewModel>();
            foreach (var cer in GetCertificatesByEmployeerId(id))
            {
                model.Add(new CertificateViewModel()
                {
                    Id = cer.Id,
                    Path = cer.Path,
                    Title = cer.Title,
                    EndDateCer = cer.EndDate,
                    StartDateCer = cer.StartDate
                });
            }

            return model;
        }

        //public string GetArray(int id)
        //{
        //    string model = string.Empty;
        //    foreach (var cer in GetCertificatesByEmployeerId(id))
        //    {
        //        model += String.Join(",",cer.Title,cer.Path,cer.Title,cer.CertificateValidity);
        //        model += "#";
        //    }

        //    return model;
        //}

        public void UpdateCertificateModel(List<CertificateViewModel> modelCertificates,int employeerId)
        {
            List<Certificate> certificates = GetCertificatesByEmployeerId(employeerId).ToList();
            if (modelCertificates != null)
            {
                foreach (var cer in modelCertificates)
                {
                    Certificate certificate = certificates.FirstOrDefault(c => cer.Title == c.Title);
                    if (certificate != null)
                    {
                        certificates.Remove(certificate);
                    }else if (cer.Title != null && cer.CertificateImage != null)
                    {
                        AddEntity(new Certificate()
                        {
                            EmployeerId = employeerId,
                            Title = cer.Title,
                            Path = $"images/{employeerId}/certificates/{cer.CertificateImage.FileName}",
                            EndDate = cer.EndDateCer,
                            StartDate = cer.StartDateCer
                        });
                        AddFile(cer.CertificateImage, employeerId);
                    }
                }
            }

            foreach (var cer in certificates)
            {
                RemoveEntity(cer.Id);
            }
        }

        public List<Certificate> FutureCertificateNotification()
        {
            List<Certificate> certificate = AllEntities.Where(c =>
                c.EndDate != null &&
                ((DateTime)c.EndDate).Date > DateTime.Now.Date &&
                ((DateTime)c.EndDate).Date <= DateTime.Now.AddDays(3).Date).Include(e => e.Employeer).ToList();
            
            return certificate;
        }
        
        public List<Certificate> PastCertificateNotification()
        {
            List<Certificate> certificate = AllEntities.Where(c =>
                c.EndDate != null &&
                ((DateTime)c.EndDate).Date <= DateTime.Now.Date &&
                ((DateTime)c.EndDate).Date >= DateTime.Now.AddDays(-3).Date).Include(e => e.Employeer).ToList();
            
            return certificate;
        }

        public List<Certificate> Notifications()
        {
            return AllEntities.Where(c =>
                (((DateTime)c.EndDate).Date == DateTime.Now.AddDays(-3).Date ||
                ((DateTime)c.EndDate).Date == DateTime.Now.Date) &&
                c.EndDate != null).Include(e => e.Employeer).ToList();
        }

        public List<string> Download(int id)
        {
            Certificate certificate = GetEntityById(id);
            string type = certificate.Path.Split('.', StringSplitOptions.RemoveEmptyEntries).Last();
            string filePath = Path.Combine(environment.WebRootPath, certificate.Path);
            string fileType = "application/" + type;
            string fileName = $"{certificate.Title}." + type;
            return new List<string>() { filePath, fileType, fileName };
        }
    }
}
