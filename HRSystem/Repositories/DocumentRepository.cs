﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using HRSystem.Models;
using HRSystem.Services;
using HRSystem.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace HRSystem.Repositories
{
    public interface IDocumentRepository : IRepository<Document>
    {
        void AddDocuments(List<string> modelDocTitle, List<IFormFile> modelDocFile, int employeerId,
            List<DateTime> startDate, List<DateTime?> endate);
        List<Document> GetDocumentsByEmploueerId(int id);
        IEnumerable<DocumentViewModel> GetViewModel(int id);
        //string GetArray(int id);
        void UpdateDocumentModel(List<DocumentViewModel> modelDocuments, int modelId);
        void AddFile(IFormFile file, int id);
        void DeleteFile(int docId);
        List<Document> FutureDocumentNotification();
        List<Document> PastDocumentNotification();
        List<Document> Notifications();
        List<string> Download(int id);
    }

    public class DocumentRepository : Repository<Document>, IDocumentRepository
    {
        private readonly FileUploadService fileUploadService;
        private readonly IHostingEnvironment environment;

        public DocumentRepository(ApplicationContext context, FileUploadService fileUploadService, IHostingEnvironment environment) : base(context)
        {
            this.fileUploadService = fileUploadService;
            this.environment = environment;
            AllEntities = context.Documents;
        }

        public void AddDocuments(List<string> modelDocTitle, List<IFormFile> modelDocFile, int employeerId,
            List<DateTime> startDate, List<DateTime?> endate)
        {
            if (modelDocFile == null) return;
            int i = 0;
            foreach (var doc in modelDocTitle)
            {
                if (string.IsNullOrEmpty(doc)) continue;
                AddEntity(new Document()
                {
                    EmployeerId = employeerId,
                    Title = doc, 
                    Path = $"images/{employeerId}/documents/{modelDocFile[i].FileName}",
                    StartDate = startDate[i],
                    EndDate = endate[i]
                });
                AddFile(modelDocFile[i], employeerId);
                i++;
            }

        }

        public List<Document> GetDocumentsByEmploueerId(int id)
        {
            return AllEntities.Where(e => e.EmployeerId == id).ToList();
        }

        public IEnumerable<DocumentViewModel> GetViewModel(int id)
        {
                List<DocumentViewModel> model = new List<DocumentViewModel>();
                foreach (var cer in GetDocumentsByEmploueerId(id))
                {
                    model.Add(new DocumentViewModel()
                    {
                        Id = cer.Id,
                        Path = cer.Path,
                        Title = cer.Title,
                        EndDateDoc = cer.EndDate,
                        StartDateDoc = cer.StartDate
                    });
                }

                return model;
        }




        public void UpdateDocumentModel(List<DocumentViewModel> modelDocuments, int modelId)
        {
            List<Document> documents = GetDocumentsByEmploueerId(modelId).ToList();
            if (modelDocuments != null)
            {
                foreach (var cer in modelDocuments)
                {
                    Document document = documents.FirstOrDefault(c => cer.Title == c.Title);
                    if (document != null)
                    {
                        documents.Remove(document);
                    }
                    else if (cer.Title != null && cer.DocumentImage != null)
                    {
                        AddEntity(new Document()
                        {
                            EmployeerId = modelId,
                            Title = cer.Title,
                            Path = $"images/{modelId}/documents/{cer.DocumentImage.FileName}",
                            EndDate = cer.EndDateDoc,
                            StartDate = cer.StartDateDoc
                        });
                        AddFile(cer.DocumentImage, modelId);
                    }
                    
                }
            }

            foreach (var doc in documents)
            {
                RemoveEntity(doc.Id);
            }
        }

        public void AddFile(IFormFile file, int id)
        {
            string path = Path.Combine(
                environment.WebRootPath,
                $"images\\{id}\\documents");

            fileUploadService.Upload(path, file.FileName, file);
        }

        public void DeleteFile(int docId)
        {
            Document document = GetEntityById(docId);
            fileUploadService.FileDelete(document.Path);
            RemoveEntity(docId);
        }

        public List<Document> PastDocumentNotification()
        {
            List<Document> document = AllEntities.Where(d =>
                d.EndDate != null &&
                ((DateTime)d.EndDate).Date <= DateTime.Now.Date &&
                ((DateTime)d.EndDate).Date >= DateTime.Now.AddDays(-3).Date).Include(e => e.Employeer).ToList();

            return document;
        }

        public List<Document> Notifications()
        {
            return AllEntities.Where(d =>
                ((DateTime)d.EndDate).Date == DateTime.Now.AddDays(-3).Date ||
                ((DateTime)d.EndDate).Date == DateTime.Now.Date &&
                d.EndDate != null).Include(e => e.Employeer).ToList();
        }

        public List<string> Download(int id)
        {
            Document document = GetEntityById(id);
            string type = document.Path.Split('.', StringSplitOptions.RemoveEmptyEntries).Last();
            string filePath = Path.Combine(environment.WebRootPath, document.Path);
            string fileType = "application/" + type;
            string fileName = $"{document.Title}." + type;
            return new List<string>() {filePath, fileType, fileName};
        }

        public List<Document> FutureDocumentNotification()
        {
            List<Document> document = AllEntities.Where(d =>
                d.EndDate != null &&
                ((DateTime)d.EndDate).Date > DateTime.Now.Date &&
                ((DateTime)d.EndDate).Date <= DateTime.Now.AddDays(3).Date).Include(e => e.Employeer).ToList();

            return document;
        }
    }
}
