﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HRSystem.Models;

namespace HRSystem.Repositories
{
    public interface ISamoleKeyRepository : IRepository<SampleKey>
    {
    }
    public class SamoleKeyRepository : Repository<SampleKey>, ISamoleKeyRepository
    {
        public SamoleKeyRepository(ApplicationContext context) : base(context)
        {
            AllEntities = context.SampleKeys;
        }
    }
}
