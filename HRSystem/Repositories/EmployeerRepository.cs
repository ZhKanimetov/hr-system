﻿using HRSystem.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using HRSystem.Controllers;
using HRSystem.Services;
using HRSystem.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Protocols;
using Document = HRSystem.Models.Document;

namespace HRSystem.Repositories
{
    public interface IEmployeerRepository : IRepository<Employeer>
    {
        Employeer FindByName(string name);
        Employeer AddEmployeer(EmployeerViewModel employeer);
        EmployeerViewModel GetViewModel(int id);
        void UpdateEmployeerViewModel(EmployeerViewModel model);
        string SaveImage(IFormFile file, int id);
        void UpdatePosition(int departmentId, int positionId, int employeerId);
        string UpdateStatus(int id, string status);
        PassportViewModel GetPassport(int id);
        void UpdatePassport(PassportViewModel model);
        List<Employeer> FutureBirthDateNotification(int id);
        List<Employeer> PastBirthDateNotification(int id);
        List<Employeer> Notifications();
        Employeer FindByEmail(string email);
        Employeer Get(string id);
    }
    public class EmployeerRepository : Repository<Employeer>, IEmployeerRepository
    {
        private readonly FileUploadService fileUploadService;
        private readonly IHostingEnvironment environment;

        public EmployeerRepository(ApplicationContext context, FileUploadService fileUploadService, IHostingEnvironment environment) : base(context)
        {
            this.fileUploadService = fileUploadService;
            this.environment = environment;
            AllEntities = context.Employeers;
        }

        public override IEnumerable<Employeer> GetAllEntities()
        {
            return AllEntities.Include(e => e.Department).Include(e => e.Position).ToList();
        }
        public Employeer FindByName(string name)
        {
            return AllEntities.FirstOrDefault(u => u.Name == name);
        }

        public Employeer AddEmployeer(EmployeerViewModel model)
        {
            Employeer employeer = new Employeer()
            {
                Address = model.Address,
                Name = model.Name,
                LastName = model.LastName,
                Surname = model.Surname,
                Status = Status.AtWork.ToString(),
                BirthdayDate = model.BirthdayDate,
                DepartmentId = model.DepartmentId,
                PositionId = model.PositionId,
                isGender = model.isGender,
                isMarried = model.isMarried,
                isMilitaryService = model.isMilitaryService,
                Notification = true,
                Telephone = String.Join(',', model.Telephone),
                Email = EmailsChecker(model.Email.ToList()),
                PassportDateIn = model.PassportDateIn,
                PassportDateOut = model.PassportDateOut,
                PassportINN = model.PassportINN,
                ByWhomGiven = model.ByWhomGiven,
                EmployeePhoto = ""
            };
            employeer = AddEntity(employeer);
            if (model.EmployeePhoto == null) return employeer;
            SaveImage(model.EmployeePhoto, employeer.Id);
            return employeer;
        }

        private string EmailsChecker(List<string> emails)
        {
            string result = string.Empty;
            foreach (var email in emails)
            {
                if (Regex.IsMatch(email, @"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$"))
                {
                    result += email + ",";
                }
            }

            return result.TrimEnd(',');
        }
        public string SaveImage(IFormFile file,int id)
        {
            Employeer employeer = GetEntityById(id);
            fileUploadService.FileDelete(employeer.EmployeePhoto);
            employeer.EmployeePhoto = $"images/{employeer.Id}/photo/{file.FileName}";
            var path = Path.Combine(
                environment.WebRootPath,
                $"images\\{employeer.Id}\\photo");

            fileUploadService.Upload(path, file.FileName, file);
            Update(employeer);
            return employeer.EmployeePhoto;
        }

        public void UpdatePosition(int departmentId, int positionId, int employeerId)
        {
            Employeer employeer = GetEntityById(employeerId);
            employeer.PositionId = positionId;
            employeer.DepartmentId = departmentId;
            Update(employeer);
        }

        public string UpdateStatus(int id, string status)
        {
            Employeer employeer = GetEntityById(id);
            IEnumerable<Status> statuses = Enum.GetValues(typeof(Status)).Cast<Status>();
            foreach (var s in statuses)
            {
                if (s.ToString() != status) continue;
                employeer.Status = status;
                Update(employeer);
                switch (s)
                {
                    case Status.AtWork:
                        return "Работает";
                    case Status.Dismissed:
                        return "На отпуске";
                    case Status.OnVacation:
                        return "Не работает";
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            return "";
        }

        public PassportViewModel GetPassport(int id)
        {
            Employeer employeer = GetEntityById(id);
            return new PassportViewModel()
            {
                ByWhomGiven = employeer.ByWhomGiven,
                PassportInn = employeer.PassportINN,
                PassportDateOut = employeer.PassportDateOut,
                PassportDateIn = employeer.PassportDateIn,
                EmployeerId = id
            };
        }

        public void UpdatePassport(PassportViewModel model)
        {
            Employeer employeer = GetEntityById(model.EmployeerId);
            employeer.ByWhomGiven = model.ByWhomGiven;
            employeer.PassportDateIn = model.PassportDateIn;
            employeer.PassportDateOut = model.PassportDateOut;
            employeer.PassportINN = model.PassportInn;

            Update(employeer);
        }

        public EmployeerViewModel GetViewModel(int id)
        {
            Employeer employeer = GetEntityById(id);
            EmployeerViewModel model = new EmployeerViewModel()
            {
                Id = employeer.Id,
                Address = employeer.Address,
                Name = employeer.Name,
                LastName = employeer.LastName,
                Surname = employeer.Surname,
                Status = employeer.Status,
                BirthdayDate = employeer.BirthdayDate,
                DepartmentId = employeer.DepartmentId,
                PositionId = employeer.PositionId,
                isGender = employeer.isGender,
                isMarried = employeer.isMarried,
                isMilitaryService = employeer.isMilitaryService,
                Notification = employeer.Notification,
                PassportDateIn = employeer.PassportDateIn,
                PassportDateOut = employeer.PassportDateOut,
                PassportINN = employeer.PassportINN,
                ByWhomGiven = employeer.ByWhomGiven
            };
            return model;
        }

        public void UpdateEmployeerViewModel(EmployeerViewModel model)
        {
            Employeer employeer = GetEntityById(model.Id);

            employeer.Address = model.Address;
            employeer.Name = model.Name;
            employeer.LastName = model.LastName;
            employeer.Surname = model.Surname;
            employeer.BirthdayDate = model.BirthdayDate;
            employeer.isGender = model.isGender;
            employeer.isMarried = model.isMarried;
            employeer.isMilitaryService = model.isMilitaryService;
            employeer.PassportDateIn = model.PassportDateIn;
            employeer.PassportDateOut = model.PassportDateOut;
            employeer.PassportINN = model.PassportINN;
            employeer.ByWhomGiven = model.ByWhomGiven;
            employeer.Telephone = String.Join(',', model.Telephone);
            employeer.Email = EmailsChecker(model.Email.ToList());
            Update(employeer);
        }

        public override Employeer GetEntityById(int id)
        {
            return AllEntities.Include(e => e.Department).Include(e => e.Position).First(e => e.Id == id);
        }
        
        public List<Employeer>  FutureBirthDateNotification(int id)
        {            
            List<Employeer> employeer = AllEntities.Where(e => 
                e.BirthdayDate.Month == DateTime.Now.Month && 
                e.BirthdayDate.Day <= DateTime.Now.AddDays(3).Day && 
                e.BirthdayDate.Day > DateTime.Now.Day ).ToList();
            if (id != 0)
            {
                employeer = employeer.Where(e => e.Id == id).ToList();
            }
            return employeer;
        }

        public List<Employeer>  PastBirthDateNotification(int id)
        {            
            List<Employeer> employeer = AllEntities.Where(e => 
                e.BirthdayDate.Month == DateTime.Now.Month && 
                e.BirthdayDate.Day >= DateTime.Now.AddDays(-3).Day && 
                e.BirthdayDate.Day <= DateTime.Now.Day).ToList();
            if (id != 0)
            {
                employeer = employeer.Where(e => e.Id == id).ToList();
            }
            return employeer;
        }
        
        public List<Employeer> Notifications()
        {
            return AllEntities.Where(e =>
                e.BirthdayDate.Month == DateTime.Now.Month ||
                e.BirthdayDate.Day == DateTime.Now.AddDays(-3).Day &&
                e.BirthdayDate.Day <= DateTime.Now.Day).ToList();
        }

        public Employeer FindByEmail(string email)
        {
            return AllEntities.First(r => r.IdForPersonal == email);
        }

        public Employeer Get(string id)
        {
            return AllEntities.First(e => e.IdForPersonal == id);
        }
    }
}
