﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HRSystem.Models;
using HRSystem.ViewModels;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace HRSystem.Repositories
{
    public interface ISkillRepository : IRepository<Skill>
    {
        void AddRangeSkills(List<SkillViewModel> model, int employeerId);
        IEnumerable<Skill> GetSkillsByEmployeerId(int employeerId);
        void UpdateSkills(List<SkillViewModel> skills, int employeerId);
        List<HistoryViewModel> GetHistory(int employeerId);
        List<Employeer> FilterByRequirements(List<string> requirements);
    }

    public class SkillRepository : Repository<Skill>, ISkillRepository
    {
        private readonly IEmployeerRepository employeerRepository;

        public SkillRepository(ApplicationContext context, IEmployeerRepository employeerRepository) : base(context)
        {
            AllEntities = context.Skills;
            this.employeerRepository = employeerRepository;
        }

        public void AddRangeSkills(List<SkillViewModel> model, int employeerId)
        {
            if (model == null) return;
            foreach (var m in model)
            {
                AddEntity(new Skill()
                {
                    StartDate = m.StartDate == DateTime.MinValue ? DateTime.Now : m.StartDate,
                    RequirementId = m.RequirementId,
                    EmployeerId = employeerId,
                    Rating = m.Rating,
                    IsImportant = m.IsImportant
                });
            }
        }

        public IEnumerable<Skill> GetSkillsByEmployeerId(int employeerId)
        {
            return AllEntities.Where(s => s.EmployeerId == employeerId).Include(s => s.Requirement).ToList();
        }

        public void UpdateSkills(List<SkillViewModel> skills, int employeerId)
        {
            AddHistory(employeerId);
            if (skills == null) return;
            foreach (var m in skills)
            {
                AddEntity(new Skill()
                {
                    StartDate = m.StartDate == DateTime.MinValue ? DateTime.Now : m.StartDate,
                    RequirementId = m.RequirementId,
                    EmployeerId = employeerId,
                    Rating = m.Rating,
                    IsImportant = m.IsImportant
                });
            }
        }

        public List<HistoryViewModel> GetHistory(int employeerId)
        {
            string result = employeerRepository.GetEntityById(employeerId).History;
            if (String.IsNullOrEmpty(result)) return new List<HistoryViewModel>();
            List<HistoryViewModel> model = JsonConvert.DeserializeObject<List<HistoryViewModel>>(result);
            return model.Where(r => r != null).ToList();
        }

        private void AddHistory(int employeerId)
        {
            Employeer employeer = employeerRepository.GetEntityById(employeerId);
            List<Skill> requirements = GetSkillsByEmployeerId(employeerId).ToList();
            List<HistoryViewModel> history = new List<HistoryViewModel>();
            if (requirements.Any())
            {
                foreach (var req in requirements)
                {
                    history.Add(new HistoryViewModel()
                    {
                        DepartmentName = employeer.Department.Name,
                        PositionName = employeer.Position.Name,
                        StartDate = req.StartDate,
                        EndDate = DateTime.Now,
                        Rating = req.Rating,
                        RequirementName = req.Requirement.Name
                    });
                    RemoveEntity(req.Id);
                }
            }

            string result = JsonConvert.SerializeObject(history);
            if (string.IsNullOrEmpty(employeer.History))
            {
                employeer.History += result;
            }
            else
            {
                result = result.Remove(0, 1);
                employeer.History = employeer.History.Remove(employeer.History.Length - 1);
                employeer.History += "," + result;
            }
            employeerRepository.Update(employeer);
        }
        public List<Employeer> FilterByRequirements(List<string> requirements)
        {
            List<Employeer> employeers = employeerRepository.GetAllEntities().ToList();
            List<Employeer> result = new List<Employeer>();
            if (!requirements.Any()) return employeers;
            foreach (var employeer in employeers)
            {
                List<string> skills = GetSkillsByEmployeerId(employeer.Id).Select(r => r.Requirement.Name).ToList();
                List<string> history = GetHistory(employeer.Id).Select(r => r.RequirementName).ToList();
                if (CompareReq(skills, requirements) || CompareReq(history, requirements))
                {
                    result.Add(employeer);
                }
            }

            return result;
        }

        private bool CompareReq(List<string> from, List<string> search)
        {
            foreach (var s in search)
            {
                if (!from.Contains(s)) return false;
            }

            return true;
        }
    }
}
