﻿using HRSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRSystem.Repositories
{
   
    public interface IRequirementRepository : IRepository<Requirement>
    {       
            Requirement FindByName(string name);        
    }

    public class RequirementRepository : Repository<Requirement>, IRequirementRepository
    {
        public RequirementRepository(ApplicationContext context) : base(context)
        {
            AllEntities = context.Requirements;
        }

        public Requirement FindByName(string name)
        {
            return AllEntities.FirstOrDefault(u => u.Name == name);
        }
    }
}
