﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using HRSystem.Models;
using HRSystem.Repositories;
using Microsoft.Extensions.Configuration;

namespace HRSystem.Services
{
    public class EmailSender
    {
        public IConfiguration Configuration { get; }
        private IEmployeerRepository employeerRepository;
        private ICertificateRepository certificateRepository;
        private IDocumentRepository documentRepository;
        public List<EmailFormModel> Info { get; set; } = new List<EmailFormModel>();

        public EmailSender(IEmployeerRepository employeerRepository,
            ICertificateRepository certificateRepository,
            IDocumentRepository documentRepository,
            IConfiguration configuration)
        {
            this.employeerRepository = employeerRepository;
            this.certificateRepository = certificateRepository;
            this.documentRepository = documentRepository;
            Configuration = configuration;
        }

        public async void Send(EmailFormModel model)
        {
            try
            {
                var body = "<p>Email From: {0} ({1})</p><p>Message:</p><p>{2}</p>";
                var message = new MailMessage();
                message.To.Add(new MailAddress(model.FromEmail));
                message.From = new MailAddress(Configuration["Login"]);
                message.Subject = model.ThemeMessage;
                message.Body = string.Format(body, model.ThemeMessage, model.FromEmail, model.Message);
                message.IsBodyHtml = true;

                using (var smtp = new SmtpClient())
                {
                    var credential = new NetworkCredential
                    {
                        UserName = Configuration["Login"],
                        Password = Configuration["Password"]
                    };
                    smtp.Credentials = credential;
                    smtp.Host = "smtp.gmail.com";
                    smtp.Port = 587;
                    smtp.EnableSsl = true;
                    await smtp.SendMailAsync(message);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return;
            }
        }
        private void UpdateNotification()
        {
            Info = new List<EmailFormModel>();
            List<Document> documents = documentRepository.Notifications();
            foreach (var doc in documents)
            {
                List<string> emails = doc.Employeer.Email.Split(",", StringSplitOptions.RemoveEmptyEntries).ToList();
                foreach (var email in emails)
                {
                    Debug.Assert(doc.EndDate != null, "doc.EndDate != null");
                    Info.Add(new EmailFormModel()
                    {
                        FromEmail = email,
                        Message = $"Уважаемый {doc.Employeer.Name} дедлайн документа \"{doc.Title}\" в {((DateTime)doc.EndDate):yyyy MMMM dd}",
                        ThemeMessage = "Документ"
                    });
                }
            }

            List<Certificate> certificates = certificateRepository.Notifications();
            foreach (var cer in certificates)
            {
                List<string> emails = cer.Employeer.Email.Split(",", StringSplitOptions.RemoveEmptyEntries).ToList();
                foreach (var email in emails)
                {
                    Debug.Assert(cer.EndDate != null, "cer.EndDate != null");
                    Info.Add(new EmailFormModel()
                    {
                        FromEmail = email,
                        Message = $"Уважаемый {cer.Employeer.Name} дедлайн документа \"{cer.Title}\" в {((DateTime)cer.EndDate):yyyy MMMM dd}",
                        ThemeMessage = "Ceртификат"
                    });
                }
            }

            List<Employeer> employeers = employeerRepository.Notifications();
            foreach (var emp in employeers)
            {
                List<string> emails = emp.Email.Split(",", StringSplitOptions.RemoveEmptyEntries).ToList();
                foreach (var email in emails)
                {
                    Info.Add(new EmailFormModel()
                    {
                        FromEmail = email,
                        Message = $"С днем рождения {emp.Name}",
                        ThemeMessage = "День Рождение"
                    });
                }
            }
        }


        public void SendNotifications()
        {

            Info = new List<EmailFormModel>();
            UpdateNotification();

            //Send(new EmailFormModel()
            //{
            //    FromEmail = "zhakhar.kanimetov@gmail.com",
            //    Message = "Hello",
            //    ThemeMessage = $"{DateTime.Now}"
            //});

            //foreach (var model in Info)
            //{
            //    Send(model);
            //}
        }
        
    }
}
