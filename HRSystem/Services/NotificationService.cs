﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace HRSystem.Services
{
    public class NotificationService : HostedService
    {
        private readonly EmailSender emailSender;

        public NotificationService(EmailSender emailSender)
        {
            this.emailSender = emailSender;
        }

        protected override async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                emailSender.SendNotifications();
                await Task.Delay(TimeSpan.FromSeconds(5), cancellationToken);
            }
        }
    }
}
