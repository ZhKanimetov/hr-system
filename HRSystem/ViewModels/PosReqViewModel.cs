﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HRSystem.Models;

namespace HRSystem.ViewModels
{
    public class PosReqViewModel
    {
        public List<Requirement> AllRequirements { get; set; }
        public List<Requirement> ImpRequirements { get; set; }
        public List<Requirement> NoImpRequirements { get; set; }
        public int PositionId { get; set; }
    }
}
