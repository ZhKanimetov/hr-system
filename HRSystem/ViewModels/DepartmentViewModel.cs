﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace HRSystem.ViewModels
{
    public class DepartmentViewModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Поле должно быть заполнено!")]
        [Remote("DepartmentValidation",controller:"Validation",AdditionalFields = "Id")]
        public string Name { get; set; }
    }
}