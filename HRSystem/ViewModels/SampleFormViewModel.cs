﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using HRSystem.Models;
using Microsoft.AspNetCore.Http;

namespace HRSystem.ViewModels
{
    public class SampleFormViewModel
    {
        [Required(ErrorMessage = "Введите название ceртификата")]
        public string Name { get; set; }
        
        [Required(ErrorMessage = "Выберите файл")]
        public IFormFile SampleFile { get; set; }
        



    }
}