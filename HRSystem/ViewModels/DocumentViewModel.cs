﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace HRSystem.ViewModels
{
    public class DocumentViewModel
    {
        [Required(ErrorMessage = "Введите название документа")]
        public string Title { get; set; }
        [Required(ErrorMessage = "Выберите файл")]
        public IFormFile DocumentImage { get; set; }

        [Required(ErrorMessage = "Укажите начальную дату")]
        public DateTime StartDateDoc { get; set; }
        public DateTime? EndDateDoc { get; set; }
        public int Id { get; set; }
        public string Path { get; set; }
        public int EmployeerId { get; set; }
    }
}
