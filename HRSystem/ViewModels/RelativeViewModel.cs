﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HRSystem.ViewModels
{
    public class RelativeViewModel
    {
        [Required(ErrorMessage = "Введите ФИО родственника")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Введите статус родсвенника")]
        public string Type { get; set; }
        [Required(ErrorMessage = "Введите год рождения родственника")]
        public DateTime Birthday { get; set; }
        public int EmployeerId { get; set; }
    }
}
