﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HRSystem.ViewModels
{
    public class PositionViewModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Поле должно быть заполнено!")]
        [Remote("PositionValidation", controller: "Validation", AdditionalFields = "Id")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Надо выбрать отдел!")]
        public int DepartmentId { get; set; }
    }
}
