﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace HRSystem.ViewModels
{
    public class CertificateViewModel
    {
        [Required(ErrorMessage = "Введите название ceртификата")]
        public string Title { get; set; }
        [Required(ErrorMessage = "Выберите файл")]
        public IFormFile CertificateImage { get; set; }
        [Required(ErrorMessage = "Укажите начальную дату")]
        public DateTime StartDateCer { get; set; }
        public DateTime? EndDateCer { get; set; }

        public int Id { get; set; }
        public string Path { get; set; }
        public int EmployeerId { get; set; }
    }
}
