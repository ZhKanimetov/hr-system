﻿using System.Collections.Generic;
using HRSystem.Models;

namespace HRSystem.ViewModels
{
    public class NotificationViewModel
    {
        public List<Employeer> futureBirthdayEmployer { get; set; }
        public List<Employeer> pastBirtdayEmployer { get; set; }
        public List<Document> FutureDocuments { get; set; }
        public List<Document> PastDocuments { get; set; }
        public List<Certificate> FutureCertificates { get; set; }
        public List<Certificate> PastCertificates { get; set; }
    }
}