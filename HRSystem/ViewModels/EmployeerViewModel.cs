﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using HRSystem.Models;
using Microsoft.AspNetCore.Http;

namespace HRSystem.ViewModels
{
    public class EmployeerViewModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Введите имя")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Введите фамилию")]
        public string Surname { get; set; }
        [Required(ErrorMessage = "Введите отчество")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "Введите год рождения")]
        public DateTime BirthdayDate { get; set; }

        //public List<RelativeViewModel> Relations { get; set; }
        [Required(ErrorMessage = "Введите ФИО родственника")]
        public List<string> RelName { get; set; }
        [Required(ErrorMessage = "Введите статус родсвенника")]
        public List<string> RelType { get; set; }
        [Required(ErrorMessage = "Введите год рождения родственника")]
        public List<DateTime> RelBirhday { get; set; }

        [Required(ErrorMessage = "Введите ИНН паспорта")]
        public string PassportINN { get; set; }
        [Required(ErrorMessage = "Введите дату выдачи")]
        public DateTime PassportDateIn { get; set; }
        [Required(ErrorMessage = "Введите дату окончания")]
        public DateTime PassportDateOut { get; set; }
        [Required(ErrorMessage = "Введите кем выдан паспорт")]
        public string ByWhomGiven { get; set; }

        [Required(ErrorMessage = "Выберите пол")]
        public bool isGender { get; set; }
        [Required(ErrorMessage = "Не выбрано")]
        public bool isMilitaryService { get; set; }
        public IFormFile EmployeePhoto { get; set; }
        [Required(ErrorMessage = "Не выбрано")]
        public bool isMarried { get; set; }
        [Required(ErrorMessage = "Заполните адрес проживания")]
        public string Address { get; set; }
        [Required(ErrorMessage = "Введите телефон")]
        public IEnumerable<string> Telephone { get; set; }
        [Required(ErrorMessage = "Введите почту")]
        [RegularExpression(@"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$", ErrorMessage = "Введите коректную почту")]
        public IEnumerable<string> Email { get; set; }


        public List<DocumentViewModel> Documents { get; set; }
        [Required(ErrorMessage = "Введите название документа")]
        public List<string> DocTitle { get; set; }
        [Required(ErrorMessage = "Выберите файл")]
        public List<IFormFile> DocFile { get; set; }
        [Range(0, 100000, ErrorMessage = "Срок не может быть меньше 0")]
        public List<int> DocumentValidity { get; set; }
        [Required(ErrorMessage = "Укажите начальную дату")]
        public List<DateTime> StartDateDoc { get; set; }
        public List<DateTime?> EndDateDoc { get; set; }

        public List<CertificateViewModel> Certificates { get; set; }
        [Required(ErrorMessage = "Введите название сертификата")]
        public List<string> CerTitle { get; set; }
        [Required(ErrorMessage = "Выберите файл")]
        public List<IFormFile> CerFile { get; set; }
        [Range(0, 100000, ErrorMessage = "Срок не может быть меньше 0")]
        public List<int> CertificateValidity { get; set; }
        [Required(ErrorMessage = "Укажите начальную дату")]
        public List<DateTime> StartDateCer { get; set; }
        public List<DateTime?> EndDateCer { get; set; }

        public string Status { get; set; }
        public bool Notification { get; set; }

        public int DepartmentId { get; set; }
        public int PositionId { get; set; }

        public List<SkillViewModel> Skills { get; set; }
    }
}
