﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HRSystem.ViewModels
{
    public class RequirementViewModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Поле должно быть заполнено!")]
        [Remote("RequirementValidation", controller: "Validation", AdditionalFields = "Id")]
        public string Name { get; set; }
        [Remote("CheckValidity", controller: "Validation")]
        public int? Validaty { get; set; }
    }
}
