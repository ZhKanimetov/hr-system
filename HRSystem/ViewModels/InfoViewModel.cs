﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HRSystem.Models;

namespace HRSystem.ViewModels
{
    public class InfoViewModel
    {
        public Employeer Employeer { get; set; }
        public List<Relation> Relations { get; set; }
        public List<Certificate> Certificates { get; set; }
        public List<Document> Documents { get; set; }
        public List<Skill> Requirements { get; set; }
        public List<SampleForm> SampleForms { get; set; }
    }
}
