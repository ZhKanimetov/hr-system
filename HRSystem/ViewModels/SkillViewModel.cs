﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using HRSystem.Models;

namespace HRSystem.ViewModels
{
    public class SkillViewModel
    {
        public int RequirementId { get; set; }
        [Required(ErrorMessage = "Введите дату")]
        public DateTime StartDate { get; set; }
        [Required(ErrorMessage = "Введите оценку")]
        public int Rating { get; set; }

        public bool IsImportant { get; set; }
    }
}
