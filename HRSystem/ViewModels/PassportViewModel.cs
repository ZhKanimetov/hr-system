﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HRSystem.ViewModels
{
    public class PassportViewModel
    {
        [Required(ErrorMessage = "Введите ИНН паспорта")]
        public string PassportInn { get; set; }
        [Required(ErrorMessage = "Введите дату выдачи")]
        public DateTime PassportDateIn { get; set; }
        [Required(ErrorMessage = "Введите дату окончания")]
        public DateTime PassportDateOut { get; set; }
        [Required(ErrorMessage = "Введите кем выдан паспорт")]
        public string ByWhomGiven { get; set; }
        public int EmployeerId { get; set; }
    }
}
