﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRSystem.ViewModels
{
    public class HistoryViewModel
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string RequirementName { get; set; }
        public int Rating { get; set; }
        public string DepartmentName { get; set; }
        public string PositionName { get; set; }
    }
}
