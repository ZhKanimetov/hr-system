﻿using HRSystem.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HRSystem.Controllers;
using HRSystem.Repositories;

namespace HRSystem
{
    public class ModelData
    {
        public static async Task Start(IServiceProvider serviceProvider)
        {

            var roleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
            var userManager = serviceProvider.GetRequiredService<UserManager<User>>();
            var sampleKeyRepository = serviceProvider.GetService<ISamoleKeyRepository>();
            string[] roleNames = { "Admin","User" };
            IdentityResult roleResult;

            foreach (var roleName in roleNames)
            {
                var roleExist = await roleManager.RoleExistsAsync(roleName);
                if (!roleExist)
                {
                    roleResult = await roleManager.CreateAsync(new IdentityRole(roleName));
                }
            }


            var poweruser = new User
            {
                UserName = "sisadmin"
            };

            string userPWD = "123";
            var user = await userManager.FindByNameAsync(poweruser.UserName);

            if (user == null)
            {
                var createPowerUser = await userManager.CreateAsync(poweruser, userPWD);
                if (createPowerUser.Succeeded)
                {
                    await userManager.AddToRoleAsync(poweruser, "Admin");
                }
            }
            List<SampleKey> sampleKeys = new List<SampleKey>()
            {
                new SampleKey()
                {
                    Name = "Имя",
                    Key = "Name",
                    Value = "Name"
                },
                new SampleKey()
                {
                    Name = "Фамилия",
                    Key = "Surname",
                    Value = "Surname"
                },
                new SampleKey()
                {
                    Name = "Отчество",
                    Key = "LastName",
                    Value = "LastName"
                },
                new SampleKey()
                {
                    Name = "Дата  рождения",
                    Key = "BirthdayDate",
                    Value = "BirthdayDate"
                },
                new SampleKey()
                {
                    Name = "Пол",
                    Key = "isGender",
                    Value = "isGender"
                },
                new SampleKey()
                {
                    Name = "Семейное положение",
                    Key = "isMarried",
                    Value = "isMarried"
                },
                new SampleKey()
                {
                    Name = "Военная служба",
                    Key = "isMilitaryService",
                    Value = "isMilitaryService"
                },
                new SampleKey()
                {
                    Name = "Адрес",
                    Key = "Address",
                    Value = "Address"
                },
                new SampleKey()
                {
                    Name = "Телефоне",
                    Key = "Telephone",
                    Value = "Telephone"
                },
                new SampleKey()
                {
                    Name = "Почта",
                    Key = "Email",
                    Value = "Email"
                },
                new SampleKey()
                {
                    Name = "ИНН паспорта",
                    Key = "PassportINN",
                    Value = "PassportINN"
                },
                new SampleKey()
                {
                    Name = "Органб выдавший документ",
                    Key = "ByWhomGiven",
                    Value = "ByWhomGiven"
                },
                new SampleKey()
                {
                    Name = "Дата выдачи паспорта",
                    Key = "PassportDateIn",
                    Value = "PassportDateIn"
                },
                new SampleKey()
                {
                    Name = "Дата окончания срока дейсвия паспорта",
                    Key = "PassportDateOut",
                    Value = "PassportDateOut"
                }
            };

            bool any = sampleKeyRepository.GetAllEntities().Any();
            if (!any)
            {
                foreach (var sampleKey in sampleKeys)
                {
                    sampleKeyRepository.AddEntity(sampleKey);
                }
            }
        }
    }
}
