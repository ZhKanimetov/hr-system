﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace HRSystem.Migrations
{
    public partial class UpdateEmployeer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Relations",
                table: "Employeers");

            migrationBuilder.AddColumn<int>(
                name: "EmployeerId",
                table: "Relations",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EmployeerId",
                table: "Relations");

            migrationBuilder.AddColumn<string>(
                name: "Relations",
                table: "Employeers",
                nullable: true);
        }
    }
}
