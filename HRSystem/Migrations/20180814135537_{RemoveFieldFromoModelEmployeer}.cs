﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace HRSystem.Migrations
{
    public partial class RemoveFieldFromoModelEmployeer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Address",
                table: "Employeers");

            migrationBuilder.DropColumn(
                name: "BirthdayDate",
                table: "Employeers");

            migrationBuilder.DropColumn(
                name: "ByWhomGiven",
                table: "Employeers");

            migrationBuilder.DropColumn(
                name: "Email",
                table: "Employeers");

            migrationBuilder.DropColumn(
                name: "EmployeePhoto",
                table: "Employeers");

            migrationBuilder.DropColumn(
                name: "LastName",
                table: "Employeers");

            migrationBuilder.DropColumn(
                name: "Notification",
                table: "Employeers");

            migrationBuilder.DropColumn(
                name: "PassportDateIn",
                table: "Employeers");

            migrationBuilder.DropColumn(
                name: "PassportDateOut",
                table: "Employeers");

            migrationBuilder.DropColumn(
                name: "PassportINN",
                table: "Employeers");

            migrationBuilder.DropColumn(
                name: "Relations",
                table: "Employeers");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "Employeers");

            migrationBuilder.DropColumn(
                name: "Surname",
                table: "Employeers");

            migrationBuilder.DropColumn(
                name: "Telephone",
                table: "Employeers");

            migrationBuilder.DropColumn(
                name: "isGender",
                table: "Employeers");

            migrationBuilder.DropColumn(
                name: "isMarried",
                table: "Employeers");

            migrationBuilder.DropColumn(
                name: "isMilitaryService",
                table: "Employeers");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Address",
                table: "Employeers",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "BirthdayDate",
                table: "Employeers",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "ByWhomGiven",
                table: "Employeers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "Employeers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EmployeePhoto",
                table: "Employeers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LastName",
                table: "Employeers",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Notification",
                table: "Employeers",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "PassportDateIn",
                table: "Employeers",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "PassportDateOut",
                table: "Employeers",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "PassportINN",
                table: "Employeers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Relations",
                table: "Employeers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Status",
                table: "Employeers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Surname",
                table: "Employeers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Telephone",
                table: "Employeers",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "isGender",
                table: "Employeers",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "isMarried",
                table: "Employeers",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "isMilitaryService",
                table: "Employeers",
                nullable: false,
                defaultValue: false);
        }
    }
}
