﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace HRSystem.Migrations
{
    public partial class DeleteFieldEmployeerIdFromModelSampleForm : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SampleForms_Employeers_EmployeerId",
                table: "SampleForms");

            migrationBuilder.DropIndex(
                name: "IX_SampleForms_EmployeerId",
                table: "SampleForms");

            migrationBuilder.DropColumn(
                name: "EmployeerId",
                table: "SampleForms");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "EmployeerId",
                table: "SampleForms",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_SampleForms_EmployeerId",
                table: "SampleForms",
                column: "EmployeerId");

            migrationBuilder.AddForeignKey(
                name: "FK_SampleForms_Employeers_EmployeerId",
                table: "SampleForms",
                column: "EmployeerId",
                principalTable: "Employeers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
