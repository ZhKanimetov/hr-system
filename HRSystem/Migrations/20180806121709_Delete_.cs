﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace HRSystem.Migrations
{
    public partial class Delete_ : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Positions_Departments_Department_Id",
                table: "Positions");

            migrationBuilder.RenameColumn(
                name: "Department_Id",
                table: "Positions",
                newName: "DepartmentId");

            migrationBuilder.RenameIndex(
                name: "IX_Positions_Department_Id",
                table: "Positions",
                newName: "IX_Positions_DepartmentId");

            migrationBuilder.AddForeignKey(
                name: "FK_Positions_Departments_DepartmentId",
                table: "Positions",
                column: "DepartmentId",
                principalTable: "Departments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Positions_Departments_DepartmentId",
                table: "Positions");

            migrationBuilder.RenameColumn(
                name: "DepartmentId",
                table: "Positions",
                newName: "Department_Id");

            migrationBuilder.RenameIndex(
                name: "IX_Positions_DepartmentId",
                table: "Positions",
                newName: "IX_Positions_Department_Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Positions_Departments_Department_Id",
                table: "Positions",
                column: "Department_Id",
                principalTable: "Departments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
