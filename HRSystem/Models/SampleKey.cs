﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRSystem.Models
{
    public class SampleKey : Entity
    {
        public string Value { get; set; }
        public string Key { get; set; }
        public string Name { get; set; }
    }
}
