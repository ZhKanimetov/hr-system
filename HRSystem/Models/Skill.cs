﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace HRSystem.Models
{
    public class Skill : Entity
    {
        [ForeignKey("Employeer")]
        public int EmployeerId { get; set; }
        [ForeignKey("Requirement")]
        public int RequirementId { get; set; }
        public int Rating { get; set; }
        public DateTime StartDate { get; set; }
        public bool IsImportant { get; set; }

        public Employeer Employeer { get; set; }
        public Requirement Requirement { get; set; }
    }
}
