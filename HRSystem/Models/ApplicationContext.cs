﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRSystem.Models
{
    public class ApplicationContext : IdentityDbContext<User>
    {
        public DbSet<Department> Departments { get; set; }
        public DbSet<Requirement> Requirements { get; set; }
        public DbSet<Position> Positions { get; set; }
        public DbSet<Employeer> Employeers { get; set; }
        public DbSet<PositionRequirement> PositionRequirements { get; set; }
        public DbSet<Skill> Skills { get; set; }
        public DbSet<Relation> Relations { get; set; }
        public DbSet<Document> Documents { get; set; }
        public DbSet<Certificate> Certificates { get; set; }
        public DbSet<SampleForm> SampleForms { get; set; }
        public DbSet<SampleKey> SampleKeys { get; set; }

        public ApplicationContext(DbContextOptions<ApplicationContext> options)
           : base(options)
        {
        }

        public ApplicationContext(string connectionString) : this(GetOptions(connectionString))
        {
        }

        private static DbContextOptions<ApplicationContext> GetOptions(string connectionString)
        {
            return SqlServerDbContextOptionsExtensions.UseSqlServer(new DbContextOptionsBuilder<ApplicationContext>(), connectionString).Options;
        }
    }
}
