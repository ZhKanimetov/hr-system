﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace HRSystem.Models
{
    public class PositionRequirement : Entity
    {
        [ForeignKey("Position")]
        public int PositionId { get; set; }
        [ForeignKey("Requirement")]
        public int RequirementId { get; set; }
        public bool IsImportant { get; set; }
        [NotMapped]
        public int Rating { get; set; }

        public Position Position { get; set; }
        public Requirement Requirement { get; set; }
    }
}
