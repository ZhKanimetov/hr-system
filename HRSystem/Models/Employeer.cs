﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HRSystem.Models
{
    public class Employeer : Entity
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string LastName { get; set; }
        public DateTime BirthdayDate { get; set; }
        
        public string PassportINN { get; set; }
        public DateTime PassportDateIn { get; set; }
        public DateTime PassportDateOut { get; set; }
        public string ByWhomGiven { get; set; }
        public string EmployeePhoto { get; set; }

        public bool isGender { get; set; }
        public bool isMilitaryService { get; set; }
        
        public bool isMarried { get; set; }
        
        public string Address { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
        public  string IdForPersonal { get; set; }
        
        public string Status { get; set; }
        public bool Notification { get; set; }
        

        [ForeignKey("Department")]
        public int DepartmentId { get; set; }
        public Department Department { get; set; }

        [ForeignKey("Position")]
        public int PositionId { get; set; }
        public Position Position { get; set; }


        public string History { get; set; }
    }
}
