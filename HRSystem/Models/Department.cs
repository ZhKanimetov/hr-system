﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace HRSystem.Models
{
    public class Department : Entity
    {
        [Required(ErrorMessage = "Поле должно быть заполнено!")]
        public string Name { get; set; }
    }
}