﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRSystem.Models
{
    public class Relation : Entity
    {
        public int EmployeerId { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public DateTime Birthday { get; set; }
    }
}
