﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace HRSystem.Models
{
    public class Certificate : Entity
    {
        public string Title { get; set; }
        public string Path { get; set; }
        
        public DateTime StartDate { get; set; } = DateTime.Now;
        public DateTime? EndDate { get; set; }


        [ForeignKey("Employeer")]
        public int EmployeerId { get; set; }
        public Employeer Employeer { get; set; }
    }
}
