﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HRSystem.Models
{
    public class Position : Entity
    {
        [Required(ErrorMessage = "Поле должно быть заполнено!")]
        public string Name { get; set; }

        [ForeignKey("Department")]
        public int DepartmentId { get; set; }
        [Required(ErrorMessage = "Надо выбрать отдел!")]
        public Department Department { get; set; }
    }
}
