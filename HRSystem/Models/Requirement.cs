﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace HRSystem.Models
{
    public class Requirement : Entity
    {
        [Required(ErrorMessage = "Поле должно быть заполнено!")]
        public string Name { get; set; }
        [Remote("CheckValidity", controller: "Validation")]
        public int Validaty { get; set; }
    }
}
