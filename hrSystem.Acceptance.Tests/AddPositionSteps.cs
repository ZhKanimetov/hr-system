﻿using System;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using TechTalk.SpecFlow;

namespace hrSystem.Acceptance.Tests
{
    [Binding]
    public class AddPositionSteps
    {
        private IWebDriver driver;

        public AddPositionSteps()
        {
            driver = Driver.GetDriver;
        }


        [Given(@"захожу на ссылку ""(.*)""")]
        public void GivenЗахожуНаСсылку(string p0)
        {
            driver.FindElement(By.LinkText("Администрирование")).Click();
        }

        [Given(@"нажимаю на вкладку ""(.*)""")]
        public void GivenНажимаюНаВкладку(string p0)
        {
            driver.FindElement(By.Id("pos")).Click();
        }

        [Given(@"нажимаю на ""(.*)""")]
        public void GivenНажимаюНа(string p0)
        {
            IWebElement button = driver.FindElement(By.XPath("//*[@id='content']/div[1]/div/button"));
            button.Click();
        }

        [Then(@"отображается модальное окно")]
        public void ThenОтображаетсяМодальноеОкно()
        {
            IWebElement input = driver.FindElement(By.XPath("//*[@id='Name']"));

            Assert.NotNull(input);
        }

        [Then(@"заполняю поля данными")]
        public void ThenЗаполняюПоляДанными()
        {
            IWebElement input = driver.FindElement(By.XPath("//*[@id='Name']"));
            var wait = new WebDriverWait(driver, TimeSpan.FromMinutes(1));
            var clickableElement = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@id='Name']")));
            Actions builder = new Actions(driver);
            builder.MoveToElement(input).SendKeys(input, "Manager").Build().Perform();

            IWebElement select = driver.FindElement(By.XPath("//*[@id='DepartmentId']"));
            var waiting = new WebDriverWait(driver, TimeSpan.FromMinutes(1));
            var clickableSelect = waiting.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@id='DepartmentId']")));
            Actions secondBuilder = new Actions(driver);
            secondBuilder.MoveToElement(select).SendKeys(select, "Маркетинг").Build().Perform();
            Thread.Sleep(1000);
        }

        [Then(@"и нажимая кнопку  ""(.*)""")]
        public void ThenИНажимаяКнопку(string p0)
        {
            IWebElement button = driver.FindElement(By.XPath("//*[@id='modalResult']/div/div/form/div[3]/input"));
            button.Click();
        }

        [Then(@"вижу созданную должность")]
        public void ThenВижуСозданнуюДолжность()
        {
            IWebElement element = driver.FindElement(By.Id("position"));
            Assert.IsNotNull(element);
        }

        [Then(@"пишу название существующей должности")]
        public void ThenпишуНазваниеСуществующейДолжности()
        {
            IWebElement input = driver.FindElement(By.XPath("//*[@id='Name']"));
            var wait = new WebDriverWait(driver, TimeSpan.FromMinutes(1));
            var clickableElement = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@id='Name']")));
            Actions builder = new Actions(driver);
            builder.MoveToElement(input).SendKeys(input, "Manager").Build().Perform();

            IWebElement select = driver.FindElement(By.XPath("//*[@id='DepartmentId']"));
            var waiting = new WebDriverWait(driver, TimeSpan.FromMinutes(1));
            var clickableSelect = waiting.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@id='DepartmentId']")));
            Actions secondBuilder = new Actions(driver);
            secondBuilder.MoveToElement(select).SendKeys(select, "Маркетинг").Build().Perform();
            Thread.Sleep(1000);
        }

        [Then(@"выходит ошибка ""(.*)""")]
        public void ThenВыходитОшибка(string p0)
        {
            IWebElement element = driver.FindElement(By.Id("Name-error"));
            Assert.AreEqual(element.Text,"Такая должность уже существует");
        }

    }
}
