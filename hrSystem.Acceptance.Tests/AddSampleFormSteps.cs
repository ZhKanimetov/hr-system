﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Runtime.Remoting.Channels;
using System.Threading;
using TechTalk.SpecFlow;

namespace hrSystem.Acceptance.Tests
{
    [Binding]
    public class AddSampleFormSteps
    {
        private IWebDriver driver;

        public AddSampleFormSteps()
        {
            driver = Driver.GetDriver;
        }

        [Given(@"перехожу по вкладке ""(.*)""")]
        public void GivenПерехожуПоВкладке(string p0)
        {
            driver.FindElement(By.LinkText("Шаблонные документы")).Click();
        }
        
        [Given(@"кликаю кнопку ""(.*)""")]
        public void GivenКликаюКнопку(string p0)
        {
            IWebElement button = driver.FindElement(By.XPath("/html/body/div/div[1]/div/button"));
            button.Click();
        }
        
        [Given(@"ввожу название шаблонного документа")]
        public void GivenВвожуНазваниеШаблонногоДокумента()
        {
            IWebElement input = driver.FindElement(By.XPath("//*[@id='Name']"));
            Thread.Sleep(50);
            input.SendKeys("договор");

            IWebElement input2 = driver.FindElement(By.XPath("//*[@id='updateContent']/div/div/form/div[2]/input"));
            Thread.Sleep(50);
            input2.Click();
            
            Thread.Sleep(50);
        }
        
        [Then(@"нажимаю кнопку""(.*)""")]
        public void ThenНажимаюКнопку(string p0)
        {
            IWebElement button = driver.FindElement(By.XPath("//*[@id='updateContent']/div/div/form/div[3]/input"));
            button.Click();
        }
        
        [Then(@"вижу созданный документ")]
        public void ThenВижуСозданныйДокумент()
        {
            IWebElement element = driver.FindElement(By.Id("sample_"));
            Assert.IsNotNull(element);
        }
    }
}
