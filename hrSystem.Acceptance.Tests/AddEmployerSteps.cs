﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;
using TechTalk.SpecFlow;

namespace hrSystem.Acceptance.Tests
{
    [Binding]
    public class AddEmployerSteps
    {
        private IWebDriver driver;

        public AddEmployerSteps()
        {
            driver = Driver.GetDriver;
        }

        [Given(@"перехожу на вкладку ""(.*)""")]
        public void GivenПерехожуНаВкладку(string p0)
        {
            driver.FindElement(By.LinkText("Сотрудники")).Click();
        }

        [Given(@"кликаю на кнопку ""(.*)""")]
        public void GivenКликаюНаКнопку(string p0)
        {
            IWebElement button = driver.FindElement(By.XPath("/html/body/div/div[1]/div[2]/button"));
            button.Click();
        }

        [Given(@"ввожу личные данные сотрудника")]
        public void ThenВвожуЛичныеДанныеСотрудника()
        {
            IWebElement input = driver.FindElement(By.XPath("//*[@id='Name']"));
            Thread.Sleep(50);
            input.SendKeys("Сардар");

            IWebElement input2 = driver.FindElement(By.XPath("//*[@id='Surname']"));
            Thread.Sleep(50);
            input2.SendKeys("Рахманбердиев");

            IWebElement input3 = driver.FindElement(By.XPath("//*[@id='LastName']"));
            Thread.Sleep(50);
            input3.SendKeys("Рахманбердиевич");

            IWebElement input4 = driver.FindElement(By.XPath("//*[@id='BirthdayDate']"));
            Thread.Sleep(50);
            input4.SendKeys("10.10.1985");
           
            IWebElement input5 = driver.FindElement(By.XPath(" //*[@id='Address']"));
            Thread.Sleep(50);
            input5.SendKeys("Бишкек 89/2");

            IWebElement input6 = driver.FindElement(By.XPath(" //*[@id='PassportINN']"));
            Thread.Sleep(50);
            input6.SendKeys("552255663322");

            IWebElement input7 = driver.FindElement(By.XPath(" //*[@id='ByWhomGiven']"));
            Thread.Sleep(50);
            input7.SendKeys("MKK-50");

            IWebElement input8 = driver.FindElement(By.XPath(" //*[@id='PassportDateIn']"));
            Thread.Sleep(50);
            input8.SendKeys("10.10.2010");

            IWebElement input9 = driver.FindElement(By.XPath(" //*[@id='PassportDateOut']"));
            Thread.Sleep(50);
            input9.SendKeys("10.10.2020");

            //IWebElement select = driver.FindElement(By.XPath("/html/body/div/div/div/form/div[11]/div[1]/label"));
            //Thread.Sleep(50);
            //select.SendKeys("");
            IWebElement select = driver.FindElement(By.XPath("/html/body/div/div/div/form/div[11]/div[1]/label"));
            var waiting = new WebDriverWait(driver, TimeSpan.FromMinutes(1));
            var clickableSelect = waiting.Until(ExpectedConditions.ElementIsVisible(By.XPath("/html/body/div/div/div/form/div[11]/div[1]/label")));
            Actions secondBuilder = new Actions(driver);
            secondBuilder.MoveToElement(select).SendKeys(select, "Мужчина").Build().Perform();
            Thread.Sleep(1000);

            IWebElement select2 = driver.FindElement(By.XPath("//*[@id='genderResult']/div/div[1]/label"));
            var waiting2 = new WebDriverWait(driver, TimeSpan.FromMinutes(1));
            var clickableSelect2 = waiting2.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@id='genderResult']/div/div[1]/label")));
            Actions secondBuilder2 = new Actions(driver);
            secondBuilder2.MoveToElement(select2).SendKeys(select2, "Служил").Build().Perform();
            Thread.Sleep(1000);
                
            IWebElement select3 = driver.FindElement(By.XPath("/ html / body / div / div / div / form / div[13] / div[2] / label"));
            var waiting3 = new WebDriverWait(driver, TimeSpan.FromMinutes(1));
            var clickableSelect3 = waiting3.Until(ExpectedConditions.ElementIsVisible(By.XPath("/ html / body / div / div / div / form / div[13] / div[2] / label")));
            Actions secondBuilder3 = new Actions(driver);
            secondBuilder3.MoveToElement(select3).SendKeys(select3, "Холост").Build().Perform();
            Thread.Sleep(1000);

            IWebElement input10 = driver.FindElement(By.XPath(" //*[@id='Telephone']"));
            Thread.Sleep(50);
            input10.SendKeys("0707070707");

            IWebElement input11 = driver.FindElement(By.XPath(" //*[@id='Email']"));
            Thread.Sleep(50);
            input11.SendKeys("hr-system@gmail.com");

            IWebElement input12 = driver.FindElement(By.XPath("//*[@id='input_reletive']/div/div[4]/button"));
            Thread.Sleep(50);
            input12.Click();

            IWebElement input13 = driver.FindElement(By.XPath("//*[@id='input_document']/div/button"));
            Thread.Sleep(50);
            input13.Click();

            IWebElement input14 = driver.FindElement(By.XPath("//*[@id='input_certificate']/div/button"));
            Thread.Sleep(50);
            input14.Click();
        }

        [Then(@"нажимая на кнопку""(.*)""")]
        public void ThenНажимаяНаКнопку(string p0)
        {
            IWebElement button = driver.FindElement(By.XPath("/html/body/div/div/div/form/div[23]/input"));
            button.Click();
        }

        [Then(@"вижу созданного сотрудника")]
        public void ThenВижуСозданногоСотрудника()
        {
            IWebElement element = driver.FindElement(By.Id("status"));
            Assert.IsNotNull(element);
        }

        [Given(@"не ввожу личные данные сотрудника")]
        public void ThenНеВвожуЛичныеДанныеСотрудника()
        {
            IWebElement input = driver.FindElement(By.XPath("//*[@id='Name']"));
            Thread.Sleep(50);
            input.SendKeys("");

            IWebElement input2 = driver.FindElement(By.XPath("//*[@id='Surname']"));
            Thread.Sleep(50);
            input2.SendKeys("");

            IWebElement input3 = driver.FindElement(By.XPath("//*[@id='LastName']"));
            Thread.Sleep(50);
            input3.SendKeys("");

            IWebElement input4 = driver.FindElement(By.XPath("//*[@id='BirthdayDate']"));
            Thread.Sleep(50);
            input4.SendKeys("");

            IWebElement input5 = driver.FindElement(By.XPath(" //*[@id='Address']"));
            Thread.Sleep(50);
            input5.SendKeys("");

            IWebElement input6 = driver.FindElement(By.XPath(" //*[@id='PassportINN']"));
            Thread.Sleep(50);
            input6.SendKeys("");

            IWebElement input7 = driver.FindElement(By.XPath(" //*[@id='ByWhomGiven']"));
            Thread.Sleep(50);
            input7.SendKeys("");

            IWebElement input8 = driver.FindElement(By.XPath(" //*[@id='PassportDateIn']"));
            Thread.Sleep(50);
            input8.SendKeys("");

            IWebElement input9 = driver.FindElement(By.XPath(" //*[@id='PassportDateOut']"));
            Thread.Sleep(50);
            input9.SendKeys("");

            //IWebElement select = driver.FindElement(By.XPath("/html/body/div/div/div/form/div[11]/div[1]/label"));
            //Thread.Sleep(50);
            //select.SendKeys("");
            IWebElement select = driver.FindElement(By.XPath("/html/body/div/div/div/form/div[11]/div[1]/label"));
            var waiting = new WebDriverWait(driver, TimeSpan.FromMinutes(1));
            var clickableSelect = waiting.Until(ExpectedConditions.ElementIsVisible(By.XPath("/html/body/div/div/div/form/div[11]/div[1]/label")));
            Actions secondBuilder = new Actions(driver);
            secondBuilder.MoveToElement(select).SendKeys(select, "Мужчина").Build().Perform();
            Thread.Sleep(1000);

            IWebElement select2 = driver.FindElement(By.XPath("//*[@id='genderResult']/div/div[1]/label"));
            var waiting2 = new WebDriverWait(driver, TimeSpan.FromMinutes(1));
            var clickableSelect2 = waiting2.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@id='genderResult']/div/div[1]/label")));
            Actions secondBuilder2 = new Actions(driver);
            secondBuilder2.MoveToElement(select2).SendKeys(select2, "Служил").Build().Perform();
            Thread.Sleep(1000);

            IWebElement select3 = driver.FindElement(By.XPath("/ html / body / div / div / div / form / div[13] / div[2] / label"));
            var waiting3 = new WebDriverWait(driver, TimeSpan.FromMinutes(1));
            var clickableSelect3 = waiting3.Until(ExpectedConditions.ElementIsVisible(By.XPath("/ html / body / div / div / div / form / div[13] / div[2] / label")));
            Actions secondBuilder3 = new Actions(driver);
            secondBuilder3.MoveToElement(select3).SendKeys(select3, "Холост").Build().Perform();
            Thread.Sleep(1000);

            IWebElement input10 = driver.FindElement(By.XPath(" //*[@id='Telephone']"));
            Thread.Sleep(50);
            input10.SendKeys("");

            IWebElement input11 = driver.FindElement(By.XPath(" //*[@id='Email']"));
            Thread.Sleep(50);
            input11.SendKeys("");

            IWebElement input12 = driver.FindElement(By.XPath("//*[@id='input_reletive']/div/div[4]/button"));
            Thread.Sleep(50);
            input12.Click();

            IWebElement input13 = driver.FindElement(By.XPath("//*[@id='input_document']/div/button"));
            Thread.Sleep(50);
            input13.Click();

            IWebElement input14 = driver.FindElement(By.XPath("//*[@id='input_certificate']/div/button"));
            Thread.Sleep(50);
            input14.Click();
        }


        [Then(@"вижу ошибку")]
        public void ThenВижуОшибку()
        {
            IWebElement element = driver.FindElement(By.Id("LastName-error"));
           
            Assert.AreEqual(element.Text, "Введите отчество");

            IWebElement element2 = driver.FindElement(By.Id("Name-error"));

            Assert.AreEqual(element2.Text, "Введите имя");
        }
    }
}
