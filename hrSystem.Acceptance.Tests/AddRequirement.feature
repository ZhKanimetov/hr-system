﻿Feature: AddRequirement
	Я собираюсь добавить компетенцию
Background: Залогинеться и войти в главную страницу
@mytag
Scenario: Добавить компетенцию
	Given я нахожусь на странице авторизации
	And   я админ
	When заполняю поля и нажимаю "Войти"
	Then вижу главную страницу
	Given захожу в раздел "Администрирование"
	And захожу на страницу "Компетенции"
	And нажимаю на кнопку "Добавить"
	Then появляется модальное окно
	And заполняю поля
	Then и нажимая  "Добавить"
	And вижу созданную компетенцию
