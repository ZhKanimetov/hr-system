﻿using System;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using TechTalk.SpecFlow;

namespace hrSystem.Acceptance.Tests
{
    [Binding]
    public class AddDepartmentSteps
    {
        private IWebDriver driver;

        public AddDepartmentSteps()
        {
            driver = Driver.GetDriver;
        }
        [Given(@"перехожу на страницу ""(.*)""")]
        public void GivenПерехожуНаСтраницу(string p0)
        {
            driver.FindElement(By.LinkText("Администрирование")).Click();
        }

        [Given(@"нажимаю ссылку ""(.*)""")]
        public void GivenНажимаюСсылку(string p0)
        {
            driver.FindElement(By.Id("dep")).Click();
        }

        [Given(@"нажимаю кнопку ""(.*)""")]
        public void GivenНажимаюКнопку(string p0)
        {
            IWebElement button = driver.FindElement(By.XPath("//*[@id='content']/div[1]/div/button"));
            button.Click();
        }

        [Then(@"вижу модальное окно")]
        public void ThenВижуМодальноеОкно()
        {
            IWebElement input = driver.FindElement(By.XPath("//*[@id='Name']"));

            Assert.NotNull(input);
        }

        [Then(@"ввожу данные")]
        public void ThenВвожуДанные()
        {
            IWebElement input = driver.FindElement(By.XPath("//*[@id='Name']"));
            var wait = new WebDriverWait(driver, TimeSpan.FromMinutes(1));
            var clickableElement = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@id='Name']")));
            Actions builder = new Actions(driver);
            builder.MoveToElement(input).SendKeys(input, "department").Build().Perform();
            Thread.Sleep(1000);
            Thread.Sleep(1000);
        }

        [Then(@"нажимая ""(.*)""")]
        public void ThenНажимая(string p0)
        {
            IWebElement button = driver.FindElement(By.XPath("//*[@id='modalResult']/div/div/form/div[2]/input"));
            Actions builder = new Actions(driver);
            builder.MoveToElement(button).Click().Build().Perform();
            Thread.Sleep(1000);
        }

        [Then(@"вижу созданный отдел")]
        public void ThenВижуСозданныйОтдел()
        {
            IWebElement element = driver.FindElement(By.Id("department"));
            Assert.IsNotNull(element);

        }
        

        [Then(@"ввожу название существующей должности")]
        public void ThenввожуНазваниеСуществующейДолжности()
        {
            IWebElement input = driver.FindElement(By.XPath("//*[@id='Name']"));
            var wait = new WebDriverWait(driver, TimeSpan.FromMinutes(1));
            var clickableElement = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@id='Name']")));
            Actions builder = new Actions(driver);
            builder.MoveToElement(input).SendKeys(input, "department").Build().Perform();
            Thread.Sleep(1000);
        }

        [Then(@"вижу ошибку ""(.*)""")]
        public void ThenВижуОшибку(string p0)
        {
            IWebElement element = driver.FindElement(By.Id("Name-error"));
            Assert.AreEqual(element.Text,"Такой отдел уже существует");
        }

    }
}