﻿using System;
using NUnit.Framework;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace hrSystem.Acceptance.Tests
{
    [Binding]
    public class AuthenticationSteps
    {
        private IWebDriver driver;

        public AuthenticationSteps()
        {
            driver = Driver.GetDriver;
        }

        [Given(@"я нахожусь на странице авторизации")]
        public void GivenЯНахожусьНаСтраницеАвторизации()
        {
            driver.Navigate().GoToUrl("http://localhost:53109/");
        }

        [Given(@"я админ")]
        public void GivenЯАдмин()
        {
            IWebElement login = driver.FindElement(By.Id("login"));
            IWebElement password = driver.FindElement(By.Id("password"));
            Assert.True(login != null && password != null);
        }

        [When(@"заполняю поля и нажимаю ""(.*)""")]
        public void WhenЗаполняюПоляИНажимаю(string p0)
        {
            driver.FindElement(By.Id("login")).SendKeys("sisadmin");
            driver.FindElement(By.Id("password")).SendKeys("123");
            driver.FindElement(By.Id("button")).Click();
        }

        [Then(@"вижу главную страницу")]
        public void ThenВижуГлавнуюСтраницу()
        {
            IWebElement link = driver.FindElement(By.LinkText("Сотрудники"));

            Assert.True(link != null);
        }
    }
}
