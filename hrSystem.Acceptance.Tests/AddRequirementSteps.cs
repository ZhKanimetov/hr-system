﻿using System;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using TechTalk.SpecFlow;

namespace hrSystem.Acceptance.Tests
{
    [Binding]
    public class AddRequirmentSteps
    {
        private IWebDriver driver;

        public AddRequirmentSteps()
        {
            driver = Driver.GetDriver;
        }

        [Given(@"захожу в раздел ""(.*)""")]
        public void GivenЗахожуВРаздел(string p0)
        {
            driver.FindElement(By.LinkText("Администрирование")).Click();
        }

        [Given(@"захожу на страницу ""(.*)""")]
        public void GivenЗахожуНаСтраницу(string p0)
        {
            driver.FindElement(By.Id("req")).Click();
        }

        [Given(@"нажимаю на кнопку ""(.*)""")]
        public void GivenНажимаюНаКнопку(string p0)
        {
            IWebElement button = driver.FindElement(By.XPath("//*[@id='content']/div[1]/div/button"));
            button.Click();
        }

        [Then(@"появляется модальное окно")]
        public void ThenПоявляетсяМодальноеОкно()
        {
            IWebElement input = driver.FindElement(By.XPath("//*[@id='Name']"));

            Assert.NotNull(input);
        }

        [Then(@"заполняю поля")]
        public void ThenЗаполняюПоля()
        {
            IWebElement input = driver.FindElement(By.XPath("//*[@id='Name']"));
            Thread.Sleep(50);
            input.SendKeys("TOEFL");

            IWebElement select = driver.FindElement(By.XPath("//*[@id='Validaty']"));
            Thread.Sleep(50);
            select.SendKeys("3");
            Thread.Sleep(1000);
        }

        [Then(@"и нажимая  ""(.*)""")]
        public void ThenИНажимая(string p0)
        {
            IWebElement button = driver.FindElement(By.XPath("//*[@id='modalResult']/div/div/form/div[3]/input"));
            button.Click();
        }

        [Then(@"вижу созданную компетенцию")]
        public void ThenВижуСозданнуюКомпетенцию()
        {
            IWebElement element = driver.FindElement(By.Id("requirement"));
            Assert.IsNotNull(element);
        }
    }
}
