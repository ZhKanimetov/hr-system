﻿using System.Collections.Generic;
using HRSystem.Models;
using HRSystem.Repositories;
using Xunit;

namespace HRSystem.Tests
{
    public class PositionControllerTest
    {
        private readonly IPositionRepository positionRepository = new PositionRepository(ContextGetter.GetApplicationContext());
        private readonly IDepartmentRepository departmentRepository = new DepartmentRepository(ContextGetter.GetApplicationContext());
        
        [Fact]
        public void IndexViewResultNotNull()
        {           
            var result = positionRepository.GetAllEntities();
 
            Assert.NotNull(result);
        }
        
        [Fact]
        public void TestPositionCreation()
        {
            Department department = new Department()
            {
                Name = "IT"
            };
            
            department = departmentRepository.AddEntity(department);
            
            Position position = new Position()
            {
                Name = "c#",
                DepartmentId = department.Id
            };
            
            position = positionRepository.AddEntity(position);
            

            Position createdPosition = positionRepository.GetEntityById(position.Id);
            Department createdDepartment = departmentRepository.GetEntityById(department.Id);

            Assert.Equal(createdPosition.Id, position.Id);
            Assert.Equal(createdDepartment.Id, department.Id);
            Assert.Equal("c#", createdPosition.Name);
            Assert.Equal("IT", createdDepartment.Name);
            DeleteTestData();
        }
        
        [Fact]
        public void TestPositionEdit()
        {
            Department department = new Department()
            {
                Name = "IT"
            };
            
            departmentRepository.AddEntity(department);
            
            Position position = new Position()
            {
                Name = "c#",
                DepartmentId = department.Id
            };

            positionRepository.Update(position);
            
            Department editedDepartment = departmentRepository.GetEntityById(department.Id);
            Position editedPosition = positionRepository.GetEntityById(position.Id);

            Assert.Equal(editedPosition.Id, position.Id);
            Assert.Equal(editedDepartment.Id, department.Id);
            Assert.Equal("c#", editedPosition.Name);
            Assert.Equal("IT", editedDepartment.Name);
            DeleteTestData();
            
        }
        
        [Fact]
        public void DeleteTestData()
        {
            IEnumerable<Position> positions = positionRepository.GetAllEntities();

            foreach (Position position in positions)
            {
                positionRepository.RemoveEntity(position.Id);
            }
        }
    }

}