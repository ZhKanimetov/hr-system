﻿using System.Collections.Generic;
using HRSystem.Models;
using HRSystem.Repositories;
using Xunit;

namespace HRSystem.Tests
{
    public class RequirementControllerTest
    {
        private readonly IRequirementRepository requirementRepository = new RequirementRepository(ContextGetter.GetApplicationContext());

        [Fact]
        public void IndexViewResultNotNull()
        {           
            var result = requirementRepository.GetAllEntities();
 
            Assert.NotNull(result);
        }
        
        [Fact]
        public void TestRequirementCreation()
        {
            Requirement requirement = new Requirement()
            {
                Name = "MSSQL"
            };

            requirement = requirementRepository.AddEntity(requirement);

            Requirement createdRequirement = requirementRepository.GetEntityById(requirement.Id);

            Assert.Equal(createdRequirement.Id, requirement.Id);
            Assert.Equal("MSSQL", createdRequirement.Name);
            DeleteTestData();
        }
        
        [Fact]
        public  void TestRequirementDelete()
        {
            Requirement requirement = new Requirement()
            {
                Name = "MSSQL"
            };

            requirement = requirementRepository.AddEntity(requirement);

            requirementRepository.RemoveEntity(requirement.Id);

            Requirement deletedDepartment = requirementRepository.GetEntityById(requirement.Id);

            Assert.Null(deletedDepartment);
            DeleteTestData();
        }
        
        [Fact]
        public void TestRequirementEdit()
        {
            Requirement requirement = new Requirement()
            {
                Name = "MSSQL"
            };

            requirementRepository.AddEntity(requirement);
            
            Requirement editedRequirement = requirementRepository.GetEntityById(requirement.Id);

            Assert.Equal(editedRequirement.Id, requirement.Id);
            Assert.Equal("MSSQL", editedRequirement.Name);
            DeleteTestData();
            
        }
        [Fact]
        public void DeleteTestData()
        {
            IEnumerable<Requirement> requirements = requirementRepository.GetAllEntities();

            foreach (Requirement requirement in requirements)
            {
                requirementRepository.RemoveEntity(requirement.Id);
            }
        }
    }
}