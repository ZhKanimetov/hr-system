﻿using System.Collections.Generic;
using HRSystem.Models;
using HRSystem.Repositories;
using Xunit;

namespace HRSystem.Tests
{
    public class DepartmentControllerTest
    {
        private readonly IDepartmentRepository departmentRepository = new DepartmentRepository(ContextGetter.GetApplicationContext());
        
        [Fact]
        public void IndexViewResultNotNull()
        {           
            var result = departmentRepository.GetAllEntities();
 
            Assert.NotNull(result);
        }
        
        [Fact]
        public void TestDepartmentCreation()
        {
            Department department = new Department()
            {
                Name = "IT"
            };

            department = departmentRepository.AddEntity(department);

            Department createdDepartment = departmentRepository.GetEntityById(department.Id);

            Assert.Equal(createdDepartment.Id, department.Id);
            Assert.Equal("IT", createdDepartment.Name);
            DeleteTestData();
        }
        
        [Fact]
        public  void TestDepartmentDelete()
        {
            Department department = new Department()
            {
                Name = "IT"
            };

            department = departmentRepository.AddEntity(department);

            departmentRepository.RemoveEntity(department.Id);

            Department deletedDepartment = departmentRepository.GetEntityById(department.Id);

            Assert.Null(deletedDepartment);
            DeleteTestData();

        }

        [Fact]
        public void TestDepartmentEdit()
        {
            Department department = new Department()
            {
                Name = "IT"
            };

            departmentRepository.AddEntity(department);
            
            Department editedDepartment = departmentRepository.GetEntityById(department.Id);

            Assert.Equal(editedDepartment.Id, department.Id);
            Assert.Equal("IT", editedDepartment.Name);
            DeleteTestData();
            
        }
        [Fact]
        public void DeleteTestData()
        {
            IEnumerable<Department> departments = departmentRepository.GetAllEntities();

            foreach (Department department in departments)
            {
                departmentRepository.RemoveEntity(department.Id);
            }
        }
    }

}