﻿using System;
using System.Collections.Generic;
using System.Text;
using HRSystem.Models;

namespace HRSystem.Tests
{
    public class ContextGetter
    {
        private static ApplicationContext context;

        public static ApplicationContext GetApplicationContext()
        {
            return context ?? (context = new ApplicationContext(
                       @"Server=(localdb)\mssqllocaldb;Database=HRSystemTestdb;Trusted_Connection=True;MultipleActiveResultSets=true")
                   );
        }
    }
}
