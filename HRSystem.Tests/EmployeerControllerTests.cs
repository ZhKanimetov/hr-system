﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using HRSystem.Controllers;
//using HRSystem.Models;
//using HRSystem.Repositories;
//using HRSystem.Services;
//using HRSystem.ViewModels;
//using Microsoft.AspNetCore.Hosting;
//using Microsoft.AspNetCore.Hosting.Internal;
//using Microsoft.AspNetCore.Mvc;
//using Xunit;

//namespace HRSystem.Tests
//{
//    public class EmployeerControllerTests
//    {
//        private static readonly IHostingEnvironment hostingEnvironment = new HostingEnvironment();
//        private static readonly FileUploadService fileUploadService = new FileUploadService();
//        private static readonly IEmployeerRepository employeerRepository = new EmployeerRepository(ContextGetter.GetApplicationContext(),fileUploadService,hostingEnvironment);
//        private readonly IRequirementRepository requirementRepository = new RequirementRepository(ContextGetter.GetApplicationContext());
//        private readonly IDepartmentRepository departmentRepository = new DepartmentRepository(ContextGetter.GetApplicationContext());
//        private readonly IPositionRepository positionRepository = new PositionRepository(ContextGetter.GetApplicationContext());
//        private readonly IRelationRepository relationRepository = new RelationRepository(ContextGetter.GetApplicationContext());
//        private readonly ISkillRepository skillRepository = new SkillRepository(ContextGetter.GetApplicationContext(),employeerRepository);
//        //private readonly IDocumentRepository documentRepository = new DocumentRepository(ContextGetter.GetApplicationContext(), fileUploadService, hostingEnvironment);
//        //private readonly ICertificateRepository certificateRepository = new CertificateRepository(ContextGetter.GetApplicationContext(), fileUploadService, hostingEnvironment);

//        [Fact]
//        public void AddEmployeer()
//        {
//            Department department = departmentRepository.AddEntity(new Department()
//            {
//                Name = "Marketing"
//            });
//            Position position = positionRepository.AddEntity(new Position()
//            {
//                Name = "HR",
//                DepartmentId = department.Id
//            });

//            List<RelativeViewModel> relationsmodel = new List<RelativeViewModel>()
//            {
//                new RelativeViewModel()
//                {
//                    Name = "Nurlan",
//                    Type = "Father",
//                    Birthday = DateTime.MinValue
//                },
//                new RelativeViewModel()
//                {
//                    Name = "Akylai",
//                    Type = "Sister",
//                    Birthday = DateTime.MinValue
//                }
//            };

//            AddRequirements();
//            List<SkillViewModel> skillmodel = new List<SkillViewModel>()
//            {
//                new SkillViewModel()
//                {
//                    RequirementId = requirementRepository.FindByName("Eng").Id,
//                    StartDate = DateTime.Now,
//                    Rating = 5
//                },
//                new SkillViewModel()
//                {
//                    RequirementId = requirementRepository.FindByName("Ru").Id,
//                    StartDate = DateTime.Now,
//                    Rating = 4
//                }
//            };
           
//            var telephone = new List<string>()
//            {
//                "0(777)777-777",
//                "0(666)666-666"
//            };
//            var email = new List<string>()
//            {
//                "a@gmail.com",
//                "b@gmail.com",
//                "c@gmail.com"
//            };
//            var model = new EmployeerViewModel()
//            {
//                Name = "Zhakhar",
//                Surname = "Kanimetov",
//                LastName = "Nurlanovich",
//                BirthdayDate = DateTime.MinValue,
//                Address = "Djantosheva 77",
//                Telephone = telephone,
//                Email = email,
//                Status = Status.AtWork.ToString(),
//                DepartmentId = department.Id,
//                PositionId = position.Id,
//                isGender = true,
//                isMilitaryService = false,
//                isMarried = false,
//                Notification = true,
//                Relations = relationsmodel,
//                Skills = skillmodel
//            };
//            Employeer employeer = employeerRepository.AddEmployeer(model);

//            relationRepository.AddRangeRelations(model.Relations, employeer.Id);
//            List<Relation> relations = relationRepository.GetRelationsByEmployeerId(employeer.Id).ToList();

//            skillRepository.AddRangeSkills(model.Skills, employeer.Id);
//            List<Skill> skills = skillRepository.GetSkillsByEmployeerId(employeer.Id).ToList();

//            Assert.Equal(skillmodel.Count, skills.Count);
//            Assert.Equal(relationsmodel.Count, relations.Count);
//            Assert.Equal(employeer.Name, model.Name);
//            Assert.Equal(employeer.Telephone, String.Join(",", telephone));
//            Assert.Equal(employeer.PositionId, position.Id);
//            employeerRepository.RemoveEntity(employeer.Id);
//        }

//        private void AddRequirements()
//        {
//            List<Requirement> requirements = new List<Requirement>()
//            {
//                new Requirement()
//                {
//                    Name = "Eng",
//                    Validaty = 36
//                },
//                new Requirement()
//                {
//                    Name = "Ru"
//                }
//            };

//            foreach (var req in requirements)
//            {
//                requirementRepository.AddEntity(req);
//            }
//        }

//    }

//}
