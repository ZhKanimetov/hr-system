﻿using HRSystem.Models;
using HRSystem.Repositories;
using Xunit;

namespace HRSystem.Tests
{
    public class PosReqControllerTest
    {
        private readonly IDepartmentRepository departmentRepository = new DepartmentRepository(ContextGetter.GetApplicationContext());
        private readonly IPositionRepository positionRepository = new PositionRepository(ContextGetter.GetApplicationContext());
        private static readonly IRequirementRepository requirementRepository = new RequirementRepository(ContextGetter.GetApplicationContext());
        private readonly IPosReqRepository posReqRepository = new PosReqRepository(ContextGetter.GetApplicationContext(), requirementRepository);
        
        [Fact]
        public void IndexViewResultNotNull()
        {           
            var result = posReqRepository.GetAllEntities();
 
            Assert.NotNull(result);
        }
        
        [Fact]
        public void TestPosReqCreation()
        {
            Department department = new Department()                    
            {                                                           
                Name = "IT"                                             
            };                                                          
                                                            
            department = departmentRepository.AddEntity(department);    
            
            Position position = new Position()
            {
                Name = "c#",
                DepartmentId = department.Id
            };
            
            position = positionRepository.AddEntity(position);
            
            Requirement requirement = new Requirement()
            {
                Name = "MSSQL"
            };

            requirement = requirementRepository.AddEntity(requirement);
            
            PositionRequirement positionRequirement = new PositionRequirement()
            {
                PositionId = position.Id,
                RequirementId = requirement.Id,
                IsImportant = true
            };
            
            positionRequirement = posReqRepository.AddEntity(positionRequirement);
            
            Department createdDepartment = departmentRepository.GetEntityById(department.Id);
            Position createdPosition = positionRepository.GetEntityById(position.Id);
            Requirement createdRequirement = requirementRepository.GetEntityById(requirement.Id);
            PositionRequirement createdPosReq = posReqRepository.GetEntityById(positionRequirement.Id);
            
            Assert.Equal(createdDepartment.Id, department.Id); 
            Assert.Equal(createdDepartment.Name, "IT");
            Assert.Equal(createdPosition.Id, position.Id);
            Assert.Equal(createdPosition.Name, "c#");
            Assert.Equal(createdRequirement.Id, requirement.Id);
            Assert.Equal(createdRequirement.Name, "MSSQL");
            Assert.Equal(createdPosReq.Id, positionRequirement.Id);
        }
    }
}